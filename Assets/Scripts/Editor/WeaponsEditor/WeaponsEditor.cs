using Assets.Scripts.Gameplay.CoreGameplay.Combat.Data;
using Assets.Scripts.Gameplay.Weapons.Data;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

namespace Assets.Scripts.Editor.WeaponsEditor
{
    public class WeaponsEditor : OdinMenuEditorWindow
    {
        [MenuItem("Tools/Weapon Settings")]
        private static void OpenWindow()
        {
            GetWindow<WeaponsEditor>().Show();
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree();

            tree.Add("Create", new WeaponCreator());

            var weaponDatas = AssetDatabase.LoadAssetAtPath<WeaponsSettings>(WeaponsSettings.ASSET_PATH);

            if (weaponDatas != null)
            {
                foreach (var weaponData in weaponDatas.playerWeapons)
                {
                    tree.Add(weaponData.name, new WeaponView(weaponData));
                }
            }

            return tree;
        }

        private void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}