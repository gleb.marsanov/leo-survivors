﻿using Assets.Scripts.Gameplay.CoreGameplay.Combat.Data;
using Assets.Scripts.Gameplay.Vfx.Views;
using Assets.Scripts.Gameplay.Weapons.Data;
using Sirenix.OdinInspector;
using UnityEditor;

namespace Assets.Scripts.Editor.WeaponsEditor
{
    internal class WeaponView
    {
        [InlineEditor(ObjectFieldMode = InlineEditorObjectFieldModes.Hidden)]
        public Weapon weapon;

        public WeaponView(Weapon weapon)
        {
            this.weapon = weapon;
            foreach (var weaponLevel in weapon.levels)
            {
                if (weaponLevel.prefab == null) continue;

                weaponLevel.attackVfxPrefab = weaponLevel.prefab.GetComponent<AttackVfx>().Value.Vfx;
                var weaponView = weaponLevel.prefab.GetComponent<Scripts.Gameplay.Weapons.Views.WeaponView>();
                weaponLevel.damage = weaponView.Value.Damage;
                weaponLevel.timeBetweenAttacks = weaponView.Value.TimeBetweenAttacks;
            }
        }

        [Button("Delete", ButtonSizes.Medium)]
        void Delete()
        {
            var weaponDatas = AssetDatabase.LoadAssetAtPath<WeaponsSettings>(WeaponsSettings.ASSET_PATH);
            weaponDatas.playerWeapons.Remove(weapon);

            AssetDatabase.RemoveObjectFromAsset(weapon);
            AssetDatabase.SaveAssets();
        }
    }
}
