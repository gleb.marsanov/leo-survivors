﻿using Assets.Scripts.Gameplay.CoreGameplay.Combat.Data;
using Assets.Scripts.Gameplay.Weapons.Data;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor.WeaponsEditor
{
    class WeaponCreator
    {
        public string name = "New Weapon";

        [InlineEditor(ObjectFieldMode = InlineEditorObjectFieldModes.Hidden)]
        public Weapon weapon;

        public WeaponCreator()
        {
            weapon = ScriptableObject.CreateInstance<Weapon>();
            weapon.guid = System.Guid.NewGuid().ToString();
        }

        [Button("Create", ButtonSizes.Medium)]
        private void Create()
        {
            var weaponDatas = AssetDatabase.LoadAssetAtPath<WeaponsSettings>(WeaponsSettings.ASSET_PATH);

            if (weaponDatas == null)
            {
                weaponDatas = ScriptableObject.CreateInstance<WeaponsSettings>();
                AssetDatabase.CreateAsset(weaponDatas, WeaponsSettings.ASSET_PATH);
            }

            foreach (var weaponLevel in weapon.levels)
            {
                weaponLevel.prefab.GetComponent<Gameplay.Weapons.Views.WeaponView>().Value.AnimationController =
                    weapon.animatorOverride;
            }

            weapon.name = name;

            weaponDatas.playerWeapons.Add(weapon);

            AssetDatabase.AddObjectToAsset(weapon, weaponDatas);

            AssetDatabase.SaveAssets();
        }
    }
}