﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIControllers;
using Assets.Scripts.UIElements.KillCounter.Controllers;
using Assets.Scripts.UIElements.Providers;
using Assets.Scripts.UIElements.Systems;
using Gamebase;
using Leopotam.EcsLite;
using SRDebugger;
using System.Collections.Generic;
using UnityEngine;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.UIElements
{
    internal class MainMenuStartup : MonoBehaviour
    {
        [SerializeField] private WeaponsSettings _weaponSettings;
        private IEnumerable<Weapon> _equippedWeapons;

        [Header("Ability Upgrading")]
        [SerializeField] private RectTransform _abilityUpgradeContainer;
        [SerializeField] private AbilityUpgrader _abilityUpgraderPrefab;

        [Header("Abilities Display")]
        [SerializeField] private GameObject _abilityPrefab;
        [SerializeField] private RectTransform _abilitiesContainer;

        [Header("Abilities Dragging")]
        [SerializeField] private DragDropManager _dragDropManager;

        private WeaponsDataParser _weaponsData;

        private EcsWorld _ecsWorld;
        private EcsSystems _initSystems;
        private EcsSystems _updateSystems;

        private List<OptionDefinition> _srDebuggerOptions = new List<OptionDefinition>();

        private AbilityUpgradeSystem _abilityUpgradeSystem;
        private WeaponSlotSystem _weaponSlotSystem;

        private void Start()
        {
            //PlayerPrefs.DeleteAll();

            _ecsWorld = new EcsWorld();

            _weaponsData = new WeaponsDataParser(_weaponSettings);

            AddSystems();
            AddSrDebuggerOptions();
        }

        private void Update()
        {
            _updateSystems.Run();
        }

        private void AddSystems()
        {
            _abilityUpgradeSystem = new AbilityUpgradeSystem(_weaponsData.Weapons, _abilityUpgradeContainer, _abilityUpgraderPrefab);
            _weaponSlotSystem = new WeaponSlotSystem(_weaponsData);
            _initSystems = new EcsSystems(_ecsWorld).ConvertScene()
                 .Add(_abilityUpgradeSystem)
                 .Add(_weaponSlotSystem)
                 .Add(new AbilityPointsDisplaySystem())
                 .Add(new ExpirienceProgressController())
                 .Add(new KillCountController());
            _initSystems.Init();


            _updateSystems = new EcsSystems(_ecsWorld)
                .Add(new WeaponAbilitiesSystem(_weaponsData, _abilityPrefab, _abilitiesContainer, _dragDropManager))
                .Add(new RemoveEventsSystem());
            _updateSystems.Init();
        }

        private void AddSrDebuggerOptions()
        {
            var option = OptionDefinition.FromMethod("Reset Player Prefs", () =>
            {
                PlayerPrefs.DeleteAll();
            });
            SRDebug.Instance.AddOption(option);
            _srDebuggerOptions.Add(option);

            option = OptionDefinition.FromMethod("Level Up", () =>
             {
                 GamebaseSystems.Instance.ProgressSystem.OpenNextLevel();
                 int availableAbilityPoints = GamebaseSystems.Instance.ResourcesSystem.Int.Get(ResourceType.AbilityPoints);
                 GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.AbilityPoints, availableAbilityPoints + 1);
             });
            SRDebug.Instance.AddOption(option);
            _srDebuggerOptions.Add(option);

            option = OptionDefinition.FromMethod("Reset Levels", () =>
            {
                GamebaseSystems.Instance.ProgressSystem.ResetProgress();
                GamebaseSystems.Instance.ProgressSystem.OpenNextLevel();
                _weaponsData.Reset();
                _abilityUpgradeSystem.Reset();
                _weaponSlotSystem.Reset();
            });
            SRDebug.Instance.AddOption(option);
            _srDebuggerOptions.Add(option);

            //option = OptionDefinition.FromMethod("Reset Kills", () =>
            //{
            //    GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.KilledEnemies, 0);
            //});
            //SRDebug.Instance.AddOption(option);
            //_srDebuggerOptions.Add(option);
        }

        private void OnDestroy()
        {
            _initSystems?.Destroy();
            _updateSystems?.Destroy();
            _ecsWorld?.Destroy();

            GamebaseSystems.Instance.GlobalEventsSystem.ClearAllEvents();
            _srDebuggerOptions.ForEach(u => SRDebug.Instance.RemoveOption(u));
            _srDebuggerOptions.Clear();

            _initSystems = null;
            _updateSystems = null;
            _ecsWorld = null;

            _weaponsData.Save();
            _weaponsData.Dispose();
        }
    }
}
