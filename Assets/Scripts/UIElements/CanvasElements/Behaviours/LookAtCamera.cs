﻿using UnityEngine;

namespace Assets.Scripts.UIElements.Components
{
    internal class LookAtCamera : MonoBehaviour
    {
        private void Start()
        {
            transform.rotation = Camera.main.transform.rotation;
            Destroy(this);
        }
    }
}
