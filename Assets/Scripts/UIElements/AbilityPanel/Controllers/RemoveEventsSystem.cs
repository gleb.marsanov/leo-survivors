using Assets.Scripts.UIElements.AbilityPanel.Events;
using Assets.Scripts.UIElements.Events;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIElements.Systems
{
    public class RemoveEventsSystem : IEcsInitSystem, IEcsRunSystem
    {
        EcsWorld _world;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
        }

        public void Run(EcsSystems systems)
        {
            ClearPool<WeaponUpgradeEvent>();
            ClearPool<AbilitiesResetEvent>();
            ClearPool<SelectWeaponEvent>();
        }

        private void ClearPool<T>() where T : struct
        {
            var filter = _world.Filter<T>().End();
            var pool = _world.GetPool<T>();
            foreach (var entity in filter)
            {
                pool.Del(entity);
            }
        }
    }
}