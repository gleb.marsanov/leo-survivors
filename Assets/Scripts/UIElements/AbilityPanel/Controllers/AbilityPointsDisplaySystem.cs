﻿using Assets.Scripts.UIElements.Components;
using Gamebase;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIElements.Systems
{
    public class AbilityPointsDisplaySystem : IEcsInitSystem
    {
        EcsPool<AbilityPointsDisplayComponent> _pool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = systems.GetWorld().Filter<AbilityPointsDisplayComponent>().End();
            _pool = systems.GetWorld().GetPool<AbilityPointsDisplayComponent>();

            int currentLevel = GamebaseSystems.Instance.ProgressSystem.CurrentProgressLevel;

            if (currentLevel == 0)
            {
                GamebaseSystems.Instance.ProgressSystem.OpenNextLevel();
                GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.AbilityPoints, 1);
            }
            int availableAbilityPoints = GamebaseSystems.Instance.ResourcesSystem.Int.Get(ResourceType.AbilityPoints);

            foreach (var entity in filter)
            {
                UpdateText(entity, availableAbilityPoints);
                _ = GamebaseSystems.Instance.ResourcesSystem.Int.Subscribe(ResourceType.AbilityPoints, (abilityPoints) =>
                  {
                      UpdateText(entity, abilityPoints);
                  });
            }
        }

        private void UpdateText(int entity, int abilityPointsCount)
        {
            try
            {
                ref var component = ref _pool.Get(entity);

                component.SetText(abilityPointsCount.ToString());
            }
            catch
            {
                //todo: исправить баг с уничтоженной entity
            }
        }
    }
}
