﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIElements.AbilityPanel.Events;
using Assets.Scripts.UIElements.Components;
using Leopotam.EcsLite;
using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using UnityEngine;

namespace Assets.Scripts.UIElements.Systems
{
    public class WeaponSlotSystem : IEcsInitSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<WeaponSlotComponent> _pool;
        private readonly WeaponsDataParser _weaponsData;

        public WeaponSlotSystem(WeaponsDataParser weaponsData)
        {
            _weaponsData = weaponsData ?? throw new ArgumentNullException();
        }

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<WeaponSlotComponent>().End();
            _pool = _world.GetPool<WeaponSlotComponent>();

            foreach (var entity in _filter)
            {
                InitSlot(entity);
            }
        }

        private void InitSlot(int entity)
        {
            ref var slot = ref _pool.Get(entity);
            slot.Weapon = _weaponsData.EquippedWeapons.ElementAt(slot.Id);
            slot.OnObjectDropped(guid => OnAbilityDropped(guid, entity));
        }

        private void OnAbilityDropped(string weaponGuid, int weaponSlotEntity)
        {
            var weapon = _weaponsData.Weapons.FirstOrDefault(weapon => weapon.guid == weaponGuid);
            if (weapon == null) throw new KeyNotFoundException();

            ref var slot = ref _pool.Get(weaponSlotEntity);
            slot.Weapon = weapon;
            _weaponsData.EquipWeapon(weapon, slot.Id);

            new SelectWeaponEvent(_world, weaponSlotEntity, weapon);
        }

        public void Reset()
        {
            foreach (var entity in _filter)
            {
                ref var slot = ref _pool.Get(entity);
                slot.Weapon = null;
            }
        }
    }
}
