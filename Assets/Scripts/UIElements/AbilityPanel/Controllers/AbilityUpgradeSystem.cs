﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIElements.Components;
using Assets.Scripts.UIElements.Events;
using Assets.Scripts.UIElements.Providers;
using Assets.Scripts.Utilities;
using Gamebase;
using Leopotam.EcsLite;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UIElements.Systems
{
    [System.Serializable]
    public class AbilityUpgradeSystem : IEcsInitSystem
    {
        private IEnumerable<Weapon> _weapons;
        private RectTransform _abilityUpgradeContainer;
        private AbilityUpgrader _abilityPrefab;
        private int _unlockedLevels;

        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<AbilityUpgradeComponent> _pool;

        public AbilityUpgradeSystem(IEnumerable<Weapon> weapons, RectTransform abilityUpgradeContainer, AbilityUpgrader abilityPrefab)
        {
            if (abilityUpgradeContainer == null) throw new ArgumentNullException();
            if (abilityPrefab == null) throw new ArgumentNullException();

            _weapons = weapons ?? throw new ArgumentNullException();
            _abilityUpgradeContainer = abilityUpgradeContainer;
            _abilityPrefab = abilityPrefab;
        }

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<AbilityUpgradeComponent>().End();
            _pool = _world.GetPool<AbilityUpgradeComponent>();
            _unlockedLevels = GetUnlockedLevels();

            InstantiateAbilities();
            SubscribeOnLevelUp();
        }

        private void InstantiateAbilities()
        {
            foreach (var weapon in _weapons)
            {
                var ability = GameObject.Instantiate(_abilityPrefab, _abilityUpgradeContainer);
                ref var abilityUpgrader = ref ability.Value;
                abilityUpgrader.weapon = weapon;
                abilityUpgrader.image.sprite = weapon.icon;
                abilityUpgrader.Update();
                abilityUpgrader.Construct(_unlockedLevels);

                if (EntityConverter.TryConvert(_world, ability.gameObject, out int entity))
                {
                    abilityUpgrader.upgradeButton.onClick.AddListener(() => UpgradeWeapon(entity));
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        private void SubscribeOnLevelUp()
        {
            GamebaseSystems.Instance.ProgressSystem.OnProgressLevelChanged += (level) =>
            {
                _unlockedLevels = GetUnlockedLevels();

                foreach (var entity in _filter)
                {
                    ref var ability = ref _pool.Get(entity);
                    ability.Construct(_unlockedLevels);
                }
            };
        }

        private int GetUnlockedLevels()
        {
            var levelsCount = 1;

            if (GamebaseSystems.Instance.ProgressSystem.FeatureIsUnlocked(GameFeatureType.WeaponLevel2))
                levelsCount++;

            if (GamebaseSystems.Instance.ProgressSystem.FeatureIsUnlocked(GameFeatureType.WeaponLevel3))
                levelsCount++;

            if (GamebaseSystems.Instance.ProgressSystem.FeatureIsUnlocked(GameFeatureType.WeaponLevel4))
                levelsCount++;

            if (GamebaseSystems.Instance.ProgressSystem.FeatureIsUnlocked(GameFeatureType.WeaponLevel5))
                levelsCount++;

            return levelsCount;
        }

        private void UpgradeWeapon(int entity)
        {
            int availableAbilityPoints = GamebaseSystems.Instance.ResourcesSystem.Int.Get(ResourceType.AbilityPoints);
            if (availableAbilityPoints <= 0) return;

            ref var ability = ref _pool.Get(entity);
            var weapon = ability.weapon;

            if (weapon.currentLevel == weapon.levels.Count) return;
            if (weapon.currentLevel == _unlockedLevels) return;

            GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.AbilityPoints, availableAbilityPoints - 1);

            weapon.currentLevel++;
            ability.Update();

            new WeaponUpgradeEvent(_world, entity, weapon);
        }

        public void Reset()
        {
            int currentLevel = GamebaseSystems.Instance.ProgressSystem.CurrentProgressLevel;
            GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.AbilityPoints, currentLevel);

            _unlockedLevels = GetUnlockedLevels();

            foreach (var entity in _filter)
            {
                ref var ability = ref _pool.Get(entity);
                ability.weapon.currentLevel = 0;
                ability.Construct(_unlockedLevels);
            }
        }
    }
}
