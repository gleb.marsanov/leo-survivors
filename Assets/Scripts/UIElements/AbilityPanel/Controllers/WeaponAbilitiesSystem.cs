﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIElements.AbilityPanel.Events;
using Assets.Scripts.UIElements.Components;
using Assets.Scripts.UIElements.Events;
using Assets.Scripts.UIElements.Providers;
using Assets.Scripts.Utilities;
using Leopotam.EcsLite;
using Sirenix.Utilities;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.UIElements.Systems
{
    [System.Serializable]
    public class WeaponAbilitiesSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<WeaponAbilityComponent> _pool;
        private GameObject _abilityPrefab;
        private RectTransform _abilitiesContainer;
        private DragDropManager _dragDropManager;
        private WeaponsDataParser _weaponsData;

        public WeaponAbilitiesSystem(WeaponsDataParser weaponsData, GameObject abilityPrefab, RectTransform abilitiesContainer, DragDropManager dragDropManager)
        {
            _weaponsData = weaponsData ?? throw new ArgumentNullException();
            _abilityPrefab = abilityPrefab ?? throw new ArgumentNullException();
            _abilitiesContainer = abilitiesContainer ?? throw new ArgumentNullException();
            _dragDropManager = dragDropManager ?? throw new ArgumentNullException();
        }

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<WeaponAbilityComponent>().End();
            _pool = _world.GetPool<WeaponAbilityComponent>();

            InstantiateAbilities();
            UpdateAbilities();
        }

        private void InstantiateAbilities()
        {
            foreach (var weapon in _weaponsData.Weapons)
            {
                var abilityObject = GameObject.Instantiate(_abilityPrefab, _abilitiesContainer).GetComponent<WeaponAbility>();
                if (abilityObject == null) throw new ArgumentNullException();

                ref var ability = ref abilityObject.Value;
                ability.weapon = weapon;
                ability.icons.ForEach(u => u.sprite = weapon.icon);
                ability.objectSettings.Id = ability.weapon.guid;

                if (!EntityConverter.TryConvert(_world, abilityObject.gameObject, out int entity))
                    throw new InvalidOperationException();

                _dragDropManager.AllObjects.Add(ability.objectSettings);
            }
        }

        public void UpdateAbilities()
        {
            foreach (var abilityEntity in _filter)
            {
                ref var ability = ref _pool.Get(abilityEntity);

                var isAbilityUnlocked = ability.weapon.currentLevel > 0;
                var isWeaponEquipped = _weaponsData.EquippedWeapons.Contains(ability.weapon);

                ability.objectSettings.gameObject.SetActive(!isWeaponEquipped && isAbilityUnlocked);
            }
        }

        public void Run(EcsSystems systems)
        {
            ListenUpgradeEvent();
            ListenSelectEvent();
        }

        private void ListenSelectEvent()
        {
            var filter = _world.Filter<SelectWeaponEvent>().End();

            if (filter.GetEntitiesCount() > 0)
            {
                UpdateAbilities();
            }
        }

        private void ListenUpgradeEvent()
        {
            var filter = _world.Filter<WeaponUpgradeEvent>().End();

            if (filter.GetEntitiesCount() > 0)
            {
                UpdateAbilities();
            }
        }
    }
}
