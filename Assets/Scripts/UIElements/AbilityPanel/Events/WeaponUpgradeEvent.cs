﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIElements.Events
{
    internal struct WeaponUpgradeEvent
    {
        public Weapon Weapon { get; private set; }

        public WeaponUpgradeEvent(EcsWorld world, int entity, Weapon weapon)
        {
            ref var evt = ref world.GetPool<WeaponUpgradeEvent>().Add(entity);
            evt.Weapon = weapon;
            this = evt;
        }
    }
}
