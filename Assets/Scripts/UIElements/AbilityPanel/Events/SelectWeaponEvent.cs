﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIElements.AbilityPanel.Events
{
    internal struct SelectWeaponEvent
    {
        public Weapon Weapon { get; private set; }
        public SelectWeaponEvent(EcsWorld world, int entity, Weapon weapon)
        {
            ref var evt = ref world.GetPool<SelectWeaponEvent>().Add(entity);
            evt.Weapon = weapon;
            this = evt;
        }
    }
}
