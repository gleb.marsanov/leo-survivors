﻿using Assets.Scripts.Gameplay.CoreGameplay.Combat.Data;
using Assets.Scripts.Gameplay.Weapons.Data;
using UnityEngine.UI;

namespace Assets.Scripts.UIElements.Components
{
    [System.Serializable]
    internal struct WeaponAbilityComponent
    {
        public Image[] icons;
        public ObjectSettings objectSettings;
        public Weapon weapon;
        public bool isEquipped;
    }
}
