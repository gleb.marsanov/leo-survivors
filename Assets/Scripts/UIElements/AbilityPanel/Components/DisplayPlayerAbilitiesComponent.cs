﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UIElements.AbilityPanel.Components
{
    [System.Serializable]
    public struct DisplayPlayerAbilitiesComponent
    {
        [SerializeField] private Image[] _abilityImages;

        public void SetImage(Sprite sprite, int index)
        {
            var abilityImage = _abilityImages[index];
            abilityImage.sprite = sprite;
            abilityImage.enabled = abilityImage.sprite != null;
        }
    }
}
