﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Doozy.Engine.Progress;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UIElements.Components
{
    [System.Serializable]
    public struct AbilityUpgradeComponent
    {
        public Image image;
        public Progressor progressor;
        public TextMeshProUGUI progressText;
        public Button upgradeButton;
        public Weapon weapon;
        private int _unlockedLevels;

        public void Construct(int unlockedLevels)
        {
            if (unlockedLevels <= 0) throw new ArgumentOutOfRangeException();

            _unlockedLevels = unlockedLevels;
            Update();
        }

        public void Update()
        {
            progressor.SetValue((float)(weapon.currentLevel) / _unlockedLevels);
            progressText.text = $"{weapon.currentLevel} / { _unlockedLevels}";
        }
    }
}
