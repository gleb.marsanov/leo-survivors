﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.UIElements.Components
{
    [System.Serializable]
    internal struct AbilityPointsDisplayComponent
    {
        [SerializeField] private TextMeshProUGUI _text;

        public void SetText(string text)
        {
            _text.text = text;
        }
    }
}
