﻿using Assets.Scripts.Gameplay.Weapons.Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UIElements.Components
{
    [System.Serializable]
    public struct WeaponSlotComponent
    {
        [SerializeField] private Image _image;
        [SerializeField] private PanelSettings _panel;
        [SerializeField] private int _id;
        private Weapon _weapon;

        public int Id => _id;

        public Weapon Weapon
        {
            get => _weapon;
            set
            {
                _weapon = value;
                SetSprite(_weapon?.icon);
            }
        }

        private void SetSprite(Sprite sprite)
        {
            _image.sprite = sprite;
            _image.enabled = _image.sprite != null;
        }

        public void OnObjectDropped(UnityAction<string> action)
        {
            _panel.OnObjectDropped.AddListener(action);
        }
    }
}
