﻿using Assets.Scripts.UIElements.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.UIElements.Providers
{
    internal class WeaponAbility : MonoProvider<WeaponAbilityComponent>
    {
        public ref WeaponAbilityComponent Value => ref value;
    }
}
