﻿using Assets.Scripts.UIElements.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.UIElements.Providers
{
    public class AbilityUpgrader : MonoProvider<AbilityUpgradeComponent>
    {
        public ref AbilityUpgradeComponent Value => ref value;
    }
}
