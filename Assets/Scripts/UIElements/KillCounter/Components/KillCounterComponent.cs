﻿using Gamebase;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UIElements.KillCounter.Components
{
    [System.Serializable]
    public struct KillCounterComponent
    {
        [SerializeField] private TextMeshProUGUI _counterText;
        private int _count;

        public void Construct(int count)
        {
            _count = count;
            _counterText.text = _count.ToString();
        }

        public void UpdateCounter()
        {
            _count++;
            _counterText.text = _count.ToString();
            GamebaseSystems.Instance.ResourcesSystem.Int.Set(ResourceType.KilledEnemies, _count);
        }
    }
}
