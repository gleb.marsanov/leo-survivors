﻿using Zenject;

namespace Assets.Scripts.UIElements.KillCounter.Resources
{
    internal class KillDataInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<KillData>().AsSingle().NonLazy();
        }
    }
}
