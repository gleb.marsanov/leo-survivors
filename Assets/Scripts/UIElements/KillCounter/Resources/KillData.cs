﻿using Gamebase;

namespace Assets.Scripts.UIElements.KillCounter.Resources
{
    public class KillData : PlayerPrefsIntResource
    {
        public int Value { get; private set; }
        public KillData(ResourcesSystem resourcesSystem) : base(resourcesSystem)
        {
            Value = resourcesSystem.Int.Get(Type);
        }

        public override ResourceType Type => ResourceType.KilledEnemies;
        protected override int DefaultValue => 0;
    }
}
