﻿using Doozy.Engine.Progress;
using Gamebase;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UIElements.ExpirienceProgress
{
    [System.Serializable]
    public struct ExpirienceProgressComponent
    {
        [SerializeField] private Progressor _progressor;
        [SerializeField] private TextMeshProUGUI _levelText;
        private int _pointsToLevelUp;

        public void Construct()
        {
            GamebaseSystems.Instance.ProgressSystem.OnXPChanged += Update;
            GamebaseSystems.Instance.ProgressSystem.OnProgressLevelChanged += UpdateLevel;

            Update(GamebaseSystems.Instance.ProgressSystem.CurrentXP);
            UpdateLevel(GamebaseSystems.Instance.ProgressSystem.CurrentProgressLevel);
        }

        private void Update(int value)
        {
            _pointsToLevelUp = GamebaseSystems.Instance.ProgressSystem.PointsToLevelUp;
            _progressor.SetValue((float)value / _pointsToLevelUp);
        }

        private void UpdateLevel(int level)
        {
            _levelText.text = level.ToString();
        }
    }
}
