﻿using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIElements.AbilityPanel.Components;
using Leopotam.EcsLite;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.UIControllers
{
    public class AbilityPanelInitController : IEcsInitSystem
    {
        private readonly IEnumerable<Weapon> _equippedWeapons;

        public AbilityPanelInitController(IEnumerable<Weapon> equippedWeapons)
        {
            _equippedWeapons = equippedWeapons;
        }

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<DisplayPlayerAbilitiesComponent>().End();
            var pool = world.GetPool<DisplayPlayerAbilitiesComponent>();

            foreach (var entity in filter)
            {
                ref var panel = ref pool.Get(entity);
                for (int i = 0; i < _equippedWeapons.Count(); i++)
                {
                    var weapon = _equippedWeapons.ElementAt(i);
                    panel.SetImage(weapon?.icon, i);
                }
            }
        }
    }
}
