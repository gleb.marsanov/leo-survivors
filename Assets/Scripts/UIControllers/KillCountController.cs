﻿using Assets.Scripts.UIElements.KillCounter.Components;
using Gamebase;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIElements.KillCounter.Controllers
{
    public class KillCountController : IEcsInitSystem
    {
        private EcsPool<KillCounterComponent> _pool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<KillCounterComponent>().End();
            _pool = world.GetPool<KillCounterComponent>();

            var killedEnemiesCount = GamebaseSystems.Instance.ResourcesSystem.Int.Get(ResourceType.KilledEnemies);

            foreach (var entity in filter)
            {
                ref var counter = ref _pool.Get(entity);
                counter.Construct(killedEnemiesCount);
                GamebaseSystems.Instance.GlobalEventsSystem.Subscribe(GlobalEventType.EnemyDeah, () => UpdateCounter(entity));
            }
        }

        private void UpdateCounter(int entity)
        {
            ref var counter = ref _pool.Get(entity);
            counter.UpdateCounter();
        }
    }
}
