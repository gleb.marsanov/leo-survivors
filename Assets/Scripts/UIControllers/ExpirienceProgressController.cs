﻿using Assets.Scripts.UIElements.ExpirienceProgress;
using Leopotam.EcsLite;

namespace Assets.Scripts.UIControllers
{
    public class ExpirienceProgressController : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<ExpirienceProgressComponent>().End();
            var pool = world.GetPool<ExpirienceProgressComponent>();

            foreach (var entity in filter)
            {
                ref var progressor = ref pool.Get(entity);
                progressor.Construct();
            }
        }
    }
}
