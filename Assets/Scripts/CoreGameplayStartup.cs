﻿using Assets.Scripts.Gameplay;
using Assets.Scripts.Gameplay.Animations.Controllers;
using Assets.Scripts.Gameplay.Combat.Controllers;
using Assets.Scripts.Gameplay.Input.Controllers;
using Assets.Scripts.Gameplay.Inventory.Controllers;
using Assets.Scripts.Gameplay.Leveling.Controllers;
using Assets.Scripts.Gameplay.Movement.Controllers;
using Assets.Scripts.Gameplay.Physics.Controllers;
using Assets.Scripts.Gameplay.Rotation.Controllers;
using Assets.Scripts.Gameplay.Spawners.Controllers;
using Assets.Scripts.Gameplay.Vfx.Controllers;
using Assets.Scripts.Gameplay.Weapons.Controllers;
using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.UIElements.KillCounter.Controllers;
using Gamebase;
using Leopotam.EcsLite;
using UnityEngine;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.UIControllers
{
    internal class CoreGameplayStartup : MonoBehaviour
    {
        [SerializeField] private WeaponsSettings _weaponsSettings;
        [SerializeField] private EnemySpawnController _enemySpawnController;

        private EcsWorld _ecsWorld;
        private EcsSystems _initSystems;
        private EcsSystems _updateSystems;
        private EcsSystems _fixedUpdateSystems;

        private WeaponsDataParser _weaponsData;

        private void Start()
        {
            _ecsWorld = new EcsWorld();

            _weaponsData = new WeaponsDataParser(_weaponsSettings);

            AddSystems();
        }

        private void Update()
        {
            _updateSystems.Run();
        }

        private void FixedUpdate()
        {
            _fixedUpdateSystems.Run();
        }

        private void AddSystems()
        {
            _initSystems = new EcsSystems(_ecsWorld).ConvertScene()
                .Add(_enemySpawnController)
                .Add(new KillCountController())
                .Add(new CollisionCheckerInitController())
                .Add(new DamageController())
                .Add(new HealthController())
                .Add(new ExpirienceRewardController())
                .Add(new PlayerWeaponsInitController(_weaponsData.EquippedWeapons))
                .Add(new WeaponAttackController())
                .Add(new ExpirienceProgressController())
                .Add(new WeaponColliderController())
                .Add(new AbilityPanelInitController(_weaponsData.EquippedWeapons));
            _initSystems.Init();

            _updateSystems = new EcsSystems(_ecsWorld)
                .Add(new MovementInputController())
                .Add(new InputWeaponSelectController());
            _updateSystems.Init();

            _fixedUpdateSystems = new EcsSystems(_ecsWorld)
                .Add(new MovementController())
                .Add(new ChaseController())
                .Add(new ForwardMovementController())

                .Add(new InventoryController())
                .Add(new EquipWeaponController())

                .Add(new RotationToDirectionController())
                .Add(new RotationToTargetController())
                .Add(new RotationToMouseController())

                .Add(new AutoAttackController())
                .Add(new TargetAutoAttackController())
                .Add(new AttackVfxController())

                .Add(new MovementAnimationController())
                .Add(new AttackAnimationController())

                .Add(new EventPoolCleanSystem());
            _fixedUpdateSystems.Init();
        }

        private void ClearWorldEntities()
        {
            for (int i = 0; i < _ecsWorld.GetAllocatedEntitiesCount(); i++)
                _ecsWorld.DelEntity(i);
        }

        private void OnDestroy()
        {
            _initSystems?.Destroy();
            _updateSystems?.Destroy();
            _fixedUpdateSystems?.Destroy();
            _ecsWorld?.Destroy();

            _initSystems = null;
            _updateSystems = null;
            _fixedUpdateSystems = null;
            _ecsWorld = null;

            GamebaseSystems.Instance.GlobalEventsSystem.ClearAllEvents();
            GamebaseSystems.Instance.ProgressSystem.OnProgressLevelChanged = null;
        }
    }
}
