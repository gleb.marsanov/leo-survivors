﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Input.Components
{
    [System.Serializable]
    internal struct InputWeaponSelectComponent
    {
        [SerializeField] private KeyCode[] _weaponSelectButtons;
        public IEnumerable<KeyCode> KeyCodes => _weaponSelectButtons;
    }
}
