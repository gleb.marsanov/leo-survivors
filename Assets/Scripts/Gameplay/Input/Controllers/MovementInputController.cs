﻿using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Gameplay.PlayerInput.Components;
using Assets.Scripts.Utilities;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Input.Controllers
{
    public class MovementInputController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<MovementComponent> _moverPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<InputDirectionComponent>().Inc<MovementComponent>().End();
            _moverPool = world.GetPool<MovementComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var mover = ref _moverPool.Get(entity);
                var inputDirection = GetInputDirection();
                mover.Direction = inputDirection.normalized;
            }
        }

        private Vector3 GetInputDirection()
        {
            var inputDirection = new Vector3(UnityEngine.Input.GetAxisRaw("Horizontal"), 0, UnityEngine.Input.GetAxisRaw("Vertical"));
            float directionAngle = MathfUtility.GetDirectionAngle(inputDirection);
            directionAngle = MathfUtility.AddCameraRotationToAngle(directionAngle);
            inputDirection = GetTargetRotationDirection(directionAngle, inputDirection);
            return inputDirection;
        }

        private Vector3 GetTargetRotationDirection(float targetAngle, Vector3 movementInput)
        {
            if (movementInput == Vector3.zero) return Vector3.zero;

            return Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
        }
    }
}
