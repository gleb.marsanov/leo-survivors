﻿using Assets.Scripts.Gameplay.Input.Components;
using Leopotam.EcsLite;
using System.Linq;
using Assets.Scripts.Gameplay.Inventory.Components;
using Assets.Scripts.Gameplay.Inventory.Events;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Input.Controllers
{
    public class InputWeaponSelectController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<InputWeaponSelectComponent> _pool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<InputWeaponSelectComponent>().Inc<InventoryComponent>().End();
            _pool = _world.GetPool<InputWeaponSelectComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var inputComponent = ref _pool.Get(entity);
                for (int i = 0; i < inputComponent.KeyCodes.Count(); i++)
                {
                    KeyCode key = inputComponent.KeyCodes.ElementAt(i);
                    if (UnityEngine.Input.GetKeyDown(key))
                    {
                        new SelectWeaponEvent(_world, entity, i);
                        break;
                    }
                }
            }
        }
    }
}
