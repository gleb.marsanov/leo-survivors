﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Behaviours
{
    internal class OnBecameInvisibleDestroy : MonoBehaviour
    {
        public Action OnInvisible;

        private void OnBecameInvisible()
        {
            Destroy(gameObject);
        }
    }
}
