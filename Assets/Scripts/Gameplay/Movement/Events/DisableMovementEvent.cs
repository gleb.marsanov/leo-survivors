﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Movement.Events
{
    internal struct DisableMovementEvent
    {
        public DisableMovementEvent(EcsWorld world, int entity)
        {
            ref var evt = ref world.GetPool<DisableMovementEvent>().Add(entity);
            this = evt;
        }
    }
}
