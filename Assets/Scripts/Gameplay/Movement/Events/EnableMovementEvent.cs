﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Movement.Events
{
    internal struct EnableMovementEvent
    {
        public EnableMovementEvent(EcsWorld world, int entity)
        {
            ref var evt = ref world.GetPool<EnableMovementEvent>().Add(entity);
            this = evt;
        }
    }
}
