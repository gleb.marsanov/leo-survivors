﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Components
{
    [System.Serializable]
    internal struct ChaseComponent
    {
        [SerializeField, Min(0)] private float _stopDistance;
        [SerializeField] private bool _chasePlayer;
        [SerializeField, HideIf(nameof(_chasePlayer))] private Transform _target;

        public float StopDistance => _stopDistance;

        public Vector3? TargetPosition
        {
            get
            {
                if (_target == null) return null;
                return _target.position;
            }
        }

        public bool ChasePlayer { get => _chasePlayer; set => _chasePlayer = value; }

        public void SetTarget(Transform target)
        {
            _target = target;
        }
    }
}
