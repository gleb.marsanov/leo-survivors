﻿using Cysharp.Threading.Tasks.Triggers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Components
{
    [System.Serializable]
    internal struct TransformComponent
    {
        [SerializeField, Required] private Transform _transform;

        public string Name => _transform.name;
        public Vector3 Position
        {
            get => _transform.position;
            set => _transform.position = value;
        }

        public Vector3 EulerAngles => _transform.eulerAngles;
        public void LookAt(Vector3 targetPosition)
        {
            _transform.LookAt(targetPosition);
        }

        internal void SetActive(bool value)
        {
            _transform.gameObject.SetActive(value);
        }

        internal void SetTransform(Transform transform)
        {
            _transform = transform;
        }

        internal void SetAsParentFor(Transform transform)
        {
            transform.SetParent(_transform);
        }

        internal GameObject InstantiateChild(GameObject gameObject)
        {
            gameObject = GameObject.Instantiate(gameObject, _transform);
            return gameObject;
        }

        internal bool IsChildOf(TransformComponent transform)
        {
            return _transform.IsChildOf(transform._transform);
        }

        internal bool IsActive => _transform.gameObject.activeInHierarchy;
        public Vector3 Forward => _transform.forward;
    }
}
