﻿using Assets.Scripts.Gameplay.Animations.Providers;
using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Components
{
    [System.Serializable]
    internal struct MovementComponent
    {
        [SerializeField, Min(0)] private float _speed;
        public bool CanMove;
        private Vector3 _direction;

        private Rigidbody _rigidBody;
        private AnimationEventListener _animationEventListener;

        public MovementComponent(float speed, Rigidbody rigidBody, AnimationEventListener animationEventListener, Vector3 direction)
        {
            if(speed<0) throw new ArgumentOutOfRangeException();
            if (rigidBody == null) throw new ArgumentNullException();
            if(animationEventListener == null) throw new ArgumentNullException();

            _speed = speed;
            _direction = direction;
            _rigidBody = rigidBody;
            _animationEventListener = animationEventListener;
            CanMove = true;
        }

        public Vector3 Direction
        {
            get => _direction;
            set
            {
                if (value.magnitude > Vector3.one.magnitude) throw new ArgumentOutOfRangeException();
                _direction = value;
            }
        }

        internal float Speed
        {
            get => _speed;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException();
                _speed = value;
            }
        }

        public Rigidbody RigidBody => _rigidBody;
        
        public Action OnDisableMovement
        {
            get => _animationEventListener.OnDisableMovement;
            set => _animationEventListener.OnDisableMovement = value;
        }

        public Action OnEnableMovement
        {
            get => _animationEventListener.OnEnableMovement;
            set => _animationEventListener.OnEnableMovement = value;
        }
    }
}
