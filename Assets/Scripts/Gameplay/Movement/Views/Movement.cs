﻿using Assets.Scripts.Gameplay.Animations.Providers;
using Assets.Scripts.Gameplay.Movement.Components;
using UnityEngine;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Movement.Views
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AnimationEventListener))]
    internal class Movement : MonoProvider<MovementComponent>
    {
        public void Construct(float speed, Vector3 direction)
        {
            value = new MovementComponent(speed, GetComponent<Rigidbody>(), GetComponent<AnimationEventListener>(), direction);
        }

        void Awake()
        {
            Construct(value.Speed, value.Direction);
        }
    }
}
