﻿using Assets.Scripts.Gameplay.Movement.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Movement.Views
{
    internal class TransformView : MonoProvider<TransformComponent>
    {
        public ref TransformComponent Value => ref value;
    }
}
