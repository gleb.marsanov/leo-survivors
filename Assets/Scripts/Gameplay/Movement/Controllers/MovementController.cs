﻿using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Controllers
{
    public class MovementController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<MovementComponent> _moverPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<MovementComponent>().End();
            _moverPool = world.GetPool<MovementComponent>();

            foreach (var entity in _filter)
            {
                ref var mover = ref _moverPool.Get(entity);
                mover.OnEnableMovement += () => EnableMovement(entity);
                mover.OnDisableMovement += () => DisableMovement(entity);
            }
        }
        
        public void Run(EcsSystems systems)
        {
            MoveEntities();
        }
        
        private void MoveEntities()
        {
            foreach (var entity in _filter)
            {
                ref var mover = ref _moverPool.Get(entity);
                if (!mover.CanMove) continue;
                
                Move(ref mover);
            }
        }
        
        private void DisableMovement(int entity)
        {
            ref var mover = ref _moverPool.Get(entity);
            mover.CanMove = false;
            mover.RigidBody.velocity = Vector3.zero;
        }

        private void EnableMovement(int entity)
        {
            ref var mover = ref _moverPool.Get(entity);
            mover.CanMove = true;
        }

        private void Move(ref MovementComponent mover)
        {
            mover.RigidBody.AddForce(mover.Direction * mover.Speed, ForceMode.VelocityChange);
            var horizontalVelocity = new Vector3(mover.RigidBody.velocity.x, 0, mover.RigidBody.velocity.z);
            horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, mover.Speed);
            mover.RigidBody.velocity = new Vector3(horizontalVelocity.x, mover.RigidBody.velocity.y, horizontalVelocity.z);
        }
    }
}
