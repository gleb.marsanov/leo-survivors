﻿using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Movement.Controllers
{
    public class ForwardMovementController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<MovementComponent> _moverPool;
        private EcsPool<TransformComponent> _transformPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<MoveForwardComponent>()
                .Inc<MovementComponent>()
                .Inc<TransformComponent>()
                .End();
            _moverPool = world.GetPool<MovementComponent>();
            _transformPool = world.GetPool<TransformComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var mover = ref _moverPool.Get(entity);
                ref var transform = ref _transformPool.Get(entity);
                if (!transform.IsActive) continue;
                mover.Direction = transform.Forward;
            }
        }
    }
}
