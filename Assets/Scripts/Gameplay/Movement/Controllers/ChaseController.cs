﻿using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;
using System.Management.Instrumentation;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Movement.Controllers
{
    public class ChaseController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<MovementComponent> _movementPool;
        private EcsPool<TransformComponent> _transformPool;
        private EcsPool<ChaseComponent> _chasePool;
        private Transform _player;

        public void Init(EcsSystems systems)
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
            if (_player == null) throw new InstanceNotFoundException();

            var world = systems.GetWorld();
            _filter = world.Filter<ChaseComponent>()
                .Inc<MovementComponent>()
                .Inc<TransformComponent>()
                .End();
            _movementPool = world.GetPool<MovementComponent>();
            _transformPool = world.GetPool<TransformComponent>();
            _chasePool = world.GetPool<ChaseComponent>();
            
            foreach (var entity in _filter)
            {
                ref var chaseComponent = ref _chasePool.Get(entity);
                if (chaseComponent.ChasePlayer)
                    chaseComponent.SetTarget(_player);
            }
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var transform = ref _transformPool.Get(entity);
                ref var chaseComponent = ref _chasePool.Get(entity);
                ref var mover = ref _movementPool.Get(entity);

                if (chaseComponent.TargetPosition == null) continue;

                var distanceToPlayer = Vector3.Distance(chaseComponent.TargetPosition.Value, transform.Position);
                mover.Direction = distanceToPlayer > chaseComponent.StopDistance ?
                    (chaseComponent.TargetPosition.Value - transform.Position).normalized : Vector3.zero;
            }
        }
    }
}
