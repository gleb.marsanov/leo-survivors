﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Physics
{
    internal class DestroyChecker : MonoBehaviour
    {
        public Action DestroyEvent;

        private void OnDestroy()
        {
            DestroyEvent?.Invoke();
        }
    }
}
