﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Physics.Events
{
    internal struct CollisionEvent
    {
        public int OtherEntity { get; private set; }

        public CollisionEvent(EcsWorld world, int entity, int otherEntity)
        {
            ref var evt = ref world.GetPool<CollisionEvent>().Add(entity);
            evt.OtherEntity = otherEntity;
            this = evt;
        }
    }
}
