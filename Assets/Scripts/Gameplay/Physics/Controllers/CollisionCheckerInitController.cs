﻿using Assets.Scripts.Gameplay.Physics.Components;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Physics.Controllers
{
    public class CollisionCheckerInitController : IEcsInitSystem
    {
        private static EcsPool<CollisionCheckerComponent> _pool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<CollisionCheckerComponent>().End();
            _pool = world.GetPool<CollisionCheckerComponent>();
            foreach (var entity in filter)
            {
                InitEntity(entity);
            }
        }

        public static void InitEntity(int entity)
        {
            ref var component = ref _pool.Get(entity);
            component.Construct(entity);
        }
    }
}
