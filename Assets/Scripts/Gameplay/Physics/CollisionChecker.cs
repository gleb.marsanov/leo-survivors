﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Physics
{
    public class CollisionChecker : MonoBehaviour
    {
        public int Entity { get; private set; }
        public Action<int> onTriggerEnter;

        public void Construct(int entity)
        {
            Entity = entity;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<CollisionChecker>(out var collisionChecker))
            {
                onTriggerEnter?.Invoke(collisionChecker.Entity);
            }
        }
    }
}
