﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Physics.Components
{
    [System.Serializable]
    public struct CollisionCheckerComponent
    {
        [SerializeField] private CollisionChecker _collisionChecker;
        public Action<int> OnTriggerEnter
        {
            get => _collisionChecker.onTriggerEnter;
            set => _collisionChecker.onTriggerEnter = value;
        }

        public void Construct(int entity)
        {
            _collisionChecker.Construct(entity);
        }
    }
}
