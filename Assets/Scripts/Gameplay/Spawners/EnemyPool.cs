﻿using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Utilities;
using Gamebase;
using Leopotam.EcsLite;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Spawners
{
    [System.Serializable]
    internal class EnemyPool
    {
        [SerializeField] private GameObject _prefab;

        [SerializeField]
        private AnimationCurve _spawnCount;

        private Queue<int> _entities = new Queue<int>();
        private EcsWorld _world;
        private EcsPool<HealthComponent> _healthPool;
        private EcsPool<TransformComponent> _transformPool;
        private EcsPool<MovementComponent> _moverPool;
        private float _spawnDistance;
        private Transform _player;

        public void Construct(EcsWorld world, float spawnDistance, Transform player)
        {
            _spawnDistance = spawnDistance;
            _player = player;
            _world = world;
            _healthPool = _world.GetPool<HealthComponent>();
            _transformPool = _world.GetPool<TransformComponent>();
            _moverPool = _world.GetPool<MovementComponent>();

            var keys = _spawnCount.keys;
            var spawnCount = keys.Max(u => u.value);

            for (int i = 0; i < spawnCount; i++)
            {
                InstantiateEnemy();
            }
        }

        public int SpawnEnemy(Vector3 position)
        {
            var enemy = _entities.Dequeue();

            ref var transform = ref _transformPool.Get(enemy);
            transform.Position = position;
            transform.SetActive(true);

            ref var health = ref _healthPool.Get(enemy);
            health.Construct();

            ref var mover = ref _moverPool.Get(enemy);
            mover.CanMove = true;

            return enemy;
        }

        public int SpawnWave(int currentPlayerLevel)
        {
            int count = Mathf.RoundToInt(_spawnCount.Evaluate(currentPlayerLevel));
            for (int i = 0; i < count; i++)
            {
                SpawnEnemy(GetSpawnPosition());
            }

            return count;
        }

        private Vector3 GetSpawnPosition()
        {
            var spawnPosition = new Vector3();

            var angle = UnityEngine.Random.Range(0, 360);
            float x = _spawnDistance * Mathf.Cos(angle);
            float z = _spawnDistance * Mathf.Sin(angle);

            spawnPosition.x = _player.position.x + x;
            spawnPosition.y = 1;
            spawnPosition.z = _player.position.z + z;

            return spawnPosition;
        }

        public void Enqueue(int entity)
        {
            _entities.Enqueue(entity);
        }

        private int InstantiateEnemy()
        {
            var enemyObject = GameObject.Instantiate(_prefab);

            if (!EntityConverter.TryConvert(_world, enemyObject, out int entity))
                throw new NullReferenceException();

            ref var health = ref _healthPool.Get(entity);
            health.OnDie += () => Enqueue(entity);
            health.OnDie += () =>
            {
                GamebaseSystems.Instance.GlobalEventsSystem.Invoke(GlobalEventType.EnemyDeah);
            };

            _entities.Enqueue(entity);
            enemyObject.SetActive(false);

            return entity;
        }
    }
}
