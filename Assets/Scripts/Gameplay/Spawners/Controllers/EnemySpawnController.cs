﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Gameplay.Movement.Components;
using Cysharp.Threading.Tasks;
using Gamebase;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Spawners.Controllers
{
    [System.Serializable]
    public class EnemySpawnController : IEcsInitSystem
    {
        [SerializeField] private bool enableSpawn;
        [SerializeField] private float _spawnDistance;
        [SerializeField] private EnemyPool[] _enemies;
        private Transform _player;
        private int _activeEntities = 0;

        public void Init(EcsSystems systems)
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;

            var world = systems.GetWorld();

            foreach (var enemyPool in _enemies)
            {
                enemyPool.Construct(world, _spawnDistance, _player);
            }

            GamebaseSystems.Instance.GlobalEventsSystem.Subscribe(GlobalEventType.EnemyDeah, OnEnemyDie);

            SpawnWave();
        }

        private async UniTask SpawnWave()
        {
            if(!enableSpawn) return;
            await UniTask.Delay(TimeSpan.FromSeconds(5));
            if(_player == null) return;
            int currentPlayerLevel = GamebaseSystems.Instance.ProgressSystem.CurrentProgressLevel;
            foreach (var enemyPool in _enemies)
            {
                _activeEntities += enemyPool.SpawnWave(currentPlayerLevel);
            }
        }

        private void OnEnemyDie()
        {
            _activeEntities--;
            if (_activeEntities <= 0)
                SpawnWave();
        }
    }
}
