﻿using Assets.Scripts.Gameplay.Combat.Controllers;
using Assets.Scripts.Gameplay.Combat.Events;
using Assets.Scripts.Gameplay.Combat.Views;
using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Gameplay.Physics;
using Assets.Scripts.Gameplay.Physics.Components;
using Assets.Scripts.Gameplay.Physics.Controllers;
using Assets.Scripts.Gameplay.Vfx.Components;
using Assets.Scripts.Gameplay.Vfx.Views;
using Assets.Scripts.Utilities;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Vfx.Controllers
{
    public class AttackVfxController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<AttackVfxComponent> _vfxComponentPool;
        private EcsPool<TransformComponent> _transformComponentPool;
        private EcsPool<AttackEvent> _attackEventPool;
        EcsPool<AttackVfxColliderComponent> _attackVfxColliderPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<AttackEvent>().Inc<TransformComponent>().Inc<AttackVfxComponent>().End();
            _vfxComponentPool = _world.GetPool<AttackVfxComponent>();
            _transformComponentPool = _world.GetPool<TransformComponent>();
            _attackEventPool = _world.GetPool<AttackEvent>();
            _attackVfxColliderPool = _world.GetPool<AttackVfxColliderComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var vfxComponent = ref _vfxComponentPool.Get(entity);
                ref var transformComponent = ref _transformComponentPool.Get(entity);
                ref var attackEvent = ref _attackEventPool.Get(entity);

                if (!transformComponent.IsActive) continue;

                GameObject vfx = vfxComponent.InstantiateVfx();

                if (vfx == null) continue;

                if (vfx.TryGetComponent<Damage>(out Damage damageComponent))
                {
                    damageComponent.Value.Damage = attackEvent.Damage;

                    if (EntityConverter.TryConvert(_world, vfx, out int vfxEntity))
                    {
                        DamageController.InitEntity(vfxEntity);

                        vfx.GetComponent<DestroyChecker>().DestroyEvent += () =>
                        {
                            _world.DelEntity(vfxEntity);
                        };

                        if (vfx.TryGetComponent<CollisionChecker>(out var collisionChecker))
                        {
                            CollisionCheckerInitController.InitEntity(vfxEntity);
                        }

                        if (vfx.TryGetComponent<AttackVfxCollider>(out var vfxCollider))
                        {
                            ref var colliderComponent = ref _attackVfxColliderPool.Get(vfxEntity);
                            colliderComponent.Construct();
                        }
                    }
                }
            }
        }
    }
}
