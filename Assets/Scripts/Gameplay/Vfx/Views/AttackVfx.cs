﻿using Assets.Scripts.Gameplay.Vfx.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Vfx.Views
{
    public class AttackVfx : MonoProvider<AttackVfxComponent>
    {
        public ref AttackVfxComponent Value => ref value;
    }
}
