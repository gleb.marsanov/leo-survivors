﻿using Cysharp.Threading.Tasks;
using Sirenix.Utilities;
using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Vfx.Components
{
    [System.Serializable]
    internal struct AttackVfxColliderComponent
    {
        [SerializeField] private Collider[] _colliders;
        [SerializeField, Min(0)] private float _timeToDisable;

        public void Construct()
        {
            DisableColliders();
        }

        private async UniTask DisableColliders()
        {
            if (_timeToDisable == 0) return;
            await UniTask.Delay(TimeSpan.FromSeconds(_timeToDisable));
            _colliders.ForEach(u => u.enabled = false);
        }
    }
}
