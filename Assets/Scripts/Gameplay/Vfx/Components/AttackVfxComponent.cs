﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Vfx.Components
{
    [System.Serializable]
    public struct AttackVfxComponent
    {
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] public GameObject Vfx;
        [SerializeField] private bool _spawnInWorldSpace;
        [SerializeField] private bool _spawnAtMousePosition;

        public GameObject InstantiateVfx()
        {
            if (Vfx == null) return null;
            var gameObject = GameObject.Instantiate(Vfx);

            if (_spawnAtMousePosition)
                gameObject.transform.position = GetMousePosition();
            else
                gameObject.transform.position = _spawnPoint.position + Vfx.transform.position;

            gameObject.transform.rotation = Quaternion.Euler(_spawnPoint.eulerAngles + Vfx.transform.eulerAngles);

            if (!_spawnInWorldSpace)
            {
                gameObject.transform.SetParent(_spawnPoint);
            }

            return gameObject;
        }

        public void SetSpawnPoint(Transform spawnPoint)
        {
            _spawnPoint = spawnPoint;
        }

        public void ShouldSpawnInWorldSpace(bool value)
        {
            _spawnInWorldSpace = value;
        }

        public void ShouldSpawnAtMousePosition(bool value)
        {
            _spawnAtMousePosition = value;
        }

        private Vector3 GetMousePosition()
        {
            Ray cameraRay = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit hit;
            if (UnityEngine.Physics.Raycast(cameraRay, out hit))
            {
                return hit.point;
            }

            return Vector3.zero;
        }
    }
}
