﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Animations.Providers
{
    internal class AnimationEventListener : MonoBehaviour
    {
        public Action OnAttack;
        public Action OnDisableMovement;
        public Action OnEnableMovement;
        public Action OnDisableWeaponCollider;
        public Action OnEnableWeaponCollider;

        public void DisableMovementEvent()
        {
            OnDisableMovement?.Invoke();
        }

        public void EnableMovementEvent()
        {
            OnEnableMovement?.Invoke();
        }

        public void AttackAnimationEvent()
        {
            OnAttack?.Invoke();
        }

        public void DisableWeaponCollider()
        {
            OnDisableWeaponCollider?.Invoke();
        }

        public void EnableWeaponCollider()
        {
            OnEnableWeaponCollider?.Invoke();
        }
    }
}
