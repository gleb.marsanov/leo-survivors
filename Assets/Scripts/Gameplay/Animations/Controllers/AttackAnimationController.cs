﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Combat.Events;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Animations.Controllers
{
    public class AttackAnimationController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<AttackAnimationComponent> _attackAnimatorPool;
        private EcsPool<AnimationComponent> _animatorPool;
        private EcsPool<AnimationEventListenerComponent> _animationEventListenerPool;
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<AttackAnimationComponent>().Inc<AnimationComponent>().Inc<AttackEvent>().End();
            _attackAnimatorPool = world.GetPool<AttackAnimationComponent>();
            _animatorPool = world.GetPool<AnimationComponent>();
            _animationEventListenerPool = world.GetPool<AnimationEventListenerComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var attackAnimator = ref _attackAnimatorPool.Get(entity);
                ref var animator = ref _animatorPool.Get(entity);
                animator.SetTrigger(attackAnimator.TriggerName);
            }
        }
    }
}
