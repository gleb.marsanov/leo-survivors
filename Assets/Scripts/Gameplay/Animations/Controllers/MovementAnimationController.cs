﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Animations.Controllers
{
    public class MovementAnimationController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<MovementAnimationComponent> _movementAnimatorsPool;
        private EcsPool<MovementComponent> _moverPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<MovementAnimationComponent>()
                .Inc<MovementComponent>()
                .End();
            _movementAnimatorsPool = world.GetPool<MovementAnimationComponent>();
            _moverPool = world.GetPool<MovementComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var movementAnimator = ref _movementAnimatorsPool.Get(entity);
                ref var mover = ref _moverPool.Get(entity);

                var animationVelocity = GetAnimationVelocity(ref movementAnimator, mover.Direction);
                movementAnimator.Speed = animationVelocity;
            }
        }

        private static Vector2 GetAnimationVelocity(ref MovementAnimationComponent movementAnimator, Vector3 direction)
        {
            var animationDirection = GetAnimationDirection(movementAnimator.ModelRotation, direction);
            var animationVelocity = Vector2.Lerp(movementAnimator.Speed, animationDirection, movementAnimator.TransitionSpeed);
            return animationVelocity;
        }

        private static Vector2 GetAnimationDirection(Vector3 transformEulerAngles, Vector3 direction)
        {
            var rotation = Quaternion.Euler(transformEulerAngles.x, transformEulerAngles.y * -1, transformEulerAngles.z);
            var animationDirection = rotation * direction;
            animationDirection.Normalize();
            return new Vector2(animationDirection.x, animationDirection.z);
        }
    }
}
