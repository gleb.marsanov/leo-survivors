﻿using Assets.Scripts.Gameplay.Animations.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Animations.Views
{
    internal class AnimationEventListenerView : MonoProvider<AnimationEventListenerComponent>
    {
        public ref AnimationEventListenerComponent Value => ref value;
    }
}
