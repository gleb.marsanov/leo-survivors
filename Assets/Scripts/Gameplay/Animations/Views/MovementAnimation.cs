﻿using Assets.Scripts.Gameplay.Animations.Components;
using UnityEngine;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Animations.Views
{
    [RequireComponent(typeof(Animator))]
    internal class MovementAnimation : MonoProvider<MovementAnimationComponent>
    {
        public void Construct(Transform modelTransform, float transitionSpeed, string speedXValueName, string speedZValueName)
        {
            value = new MovementAnimationComponent(modelTransform, transitionSpeed, speedXValueName, speedZValueName, GetComponent<Animator>());
        }

        public void Awake()
        {
            Construct(value.ModelTransform, value.TransitionSpeed, value.SpeedXValueName, value.SpeedZValueName);
        }
    }
}
