﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Animations.Components
{
    [System.Serializable]
    internal struct AttackAnimationComponent
    {
        [SerializeField] private string _attackTriggerName;

        public string TriggerName => _attackTriggerName;
    }
}
