﻿using Assets.Scripts.Gameplay.Animations.Providers;

namespace Assets.Scripts.Gameplay.Animations.Components
{
    [System.Serializable]
    internal struct AnimationEventListenerComponent
    {
        public AnimationEventListener EventListener;
    }
}
