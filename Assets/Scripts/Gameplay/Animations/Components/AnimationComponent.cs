﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Animations.Components
{
    [System.Serializable]
    internal struct AnimationComponent
    {
        [SerializeField, Required] private RuntimeAnimatorController _defaultController;
        [SerializeField, Required] private Animator _animator;
        
        public void SetFloat(string name, float value)
        {
            _animator.SetFloat(name, value);
        }

        public float GetFloat(string name)
        {
            return _animator.GetFloat(name);
        }

        public void SetTrigger(string triggerName)
        {
            _animator.SetTrigger(triggerName);
        }

        public void OverrideAnimator(AnimatorOverrideController animatorOverride)
        {
            if (animatorOverride == null)
                _animator.runtimeAnimatorController = _defaultController;
            else
                _animator.runtimeAnimatorController = animatorOverride;
        }

        internal void ResetTrigger(string triggerName)
        {
            _animator.ResetTrigger(triggerName);
        }
    }
}
