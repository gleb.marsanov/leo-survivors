﻿using System;
using ModestTree;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Animations.Components
{
    [System.Serializable]
    public struct MovementAnimationComponent
    {
        [SerializeField, Required] private Transform _modelTransform;
        [SerializeField, Range(0.1f, 1)] private float _transitionSpeed;
        [SerializeField, Required] private string _speedXValueName;
        [SerializeField, Required] private string _speedZValueName;
        private Animator _animator;

        public MovementAnimationComponent(Transform modelTransform, float transitionSpeed, string speedXValueName, string speedZValueName, Animator animator)
        {
            if (transitionSpeed < 0) throw new ArgumentOutOfRangeException();
            if (speedXValueName.IsEmpty()) throw new ArgumentException();
            if (speedZValueName.IsEmpty()) throw new ArgumentException();
            if (modelTransform == null) throw new ArgumentNullException();

            _modelTransform = modelTransform;
            _transitionSpeed = transitionSpeed;
            _speedXValueName = speedXValueName;
            _speedZValueName = speedZValueName;
            _animator = animator;
        }

        public Vector2 Speed
        {
            get => new Vector2(_animator.GetFloat(_speedXValueName), _animator.GetFloat(_speedZValueName));
            set
            {
                _animator.SetFloat(_speedXValueName, value.x);
                _animator.SetFloat(_speedZValueName, value.y);
            }
        }

        public Vector3 ModelRotation => _modelTransform.eulerAngles;
        public float TransitionSpeed => _transitionSpeed;
        public Transform ModelTransform => _modelTransform;
        public string SpeedXValueName => _speedXValueName;
        public string SpeedZValueName => _speedZValueName;
    }
}
