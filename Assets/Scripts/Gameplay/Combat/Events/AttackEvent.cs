﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Combat.Events
{
    internal struct AttackEvent
    {
        public AttackEvent(EcsWorld world, int entity, float damage)
        {
            ref var evt = ref world.GetPool<AttackEvent>().Add(entity);
            evt.Damage = damage;
            this = evt;
        }

        public float Damage { get; private set; }
    }
}
