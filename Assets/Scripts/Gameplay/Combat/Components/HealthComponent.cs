﻿using Doozy.Engine.Progress;
using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Components
{
    [System.Serializable]
    internal struct HealthComponent
    {
        [SerializeField] private float _maxHealth;
        [SerializeField] private Progressor _progressor;
        [SerializeField] private Transform _transform;
        private float _health;
        public Action OnDie;

        public float MaxHealth
        {
            get => _maxHealth;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException();
                _maxHealth = value;
            }
        }

        public float Health => _health;

        public void Construct()
        {
            _health = MaxHealth;
            UpdateProgressor();
        }

        public void TakeDamage(float damage)
        {
            var newHealth = Health - damage;
            if (newHealth <= 0)
            {
                _transform.gameObject.SetActive(false);
                OnDie?.Invoke();
                _health = 0;
            }
            else
                _health = newHealth;

            UpdateProgressor();
        }

        private void UpdateProgressor()
        {
            _progressor.SetValue(_health / _maxHealth);
        }
    }
}
