﻿namespace Assets.Scripts.Gameplay.Combat.Components
{
    [System.Serializable]
    public struct DamageComponent
    {
        public float Damage;
    }
}
