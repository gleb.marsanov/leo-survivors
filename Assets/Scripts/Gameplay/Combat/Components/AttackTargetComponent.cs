﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Components
{
    [System.Serializable]
    internal struct AttackTargetComponent
    {
        [SerializeField] private float _attackRange;
        [SerializeField, HideIf(nameof(TargetPlayer))] private Transform _target;
        [SerializeField] private bool _targetPlayer;

        public float AttackRange => _attackRange;

        public Vector3 TargetPosition => _target.position;

        public bool TargetPlayer => _targetPlayer;

        public void SetTarget(Transform transform)
        {
            _target = transform;
        }
    }
}
