﻿using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Components
{
    [System.Serializable]
    internal struct AttackComponent
    {
        [SerializeField, Min(0)] private float _timeBetweenAttacks;
        [SerializeField] private float _damage;
        private float _timeSinceLastAttack;

        public float TimeBetweenAttacks
        {
            get => _timeBetweenAttacks;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException();
                _timeBetweenAttacks = value;
            }
        }
        public float TimeSinceLastAttack
        {
            get => _timeSinceLastAttack; set
            {
                if (value < 0) throw new ArgumentOutOfRangeException();
                _timeSinceLastAttack = value;
            }
        }

        public float Damage
        {
            get => _damage;
            set => _damage = value;
        }
    }
}
