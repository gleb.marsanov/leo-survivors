﻿using Assets.Scripts.Gameplay.Combat.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Combat.Views
{
    internal class Damage : MonoProvider<DamageComponent>
    {
        public ref DamageComponent Value => ref value;
    }
}
