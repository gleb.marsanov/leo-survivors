﻿using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Tags.Components;
using Gamebase;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Controllers
{
    public class HealthController : IEcsInitSystem
    {
        private int _healthStep = 40;
        private EcsPool<HealthComponent> _healthPool;
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<HealthComponent>().End();
            _healthPool = world.GetPool<HealthComponent>();

            foreach (var entity in filter)
            {
                ref var health = ref _healthPool.Get(entity);
                health.Construct();
            }

            var playerFilter = world.Filter<HealthComponent>().Inc<PlayerTagComponent>().End();
            foreach (var entity in playerFilter)
            {
                ref var health = ref _healthPool.Get(entity);

                int playerLevel = GamebaseSystems.Instance.ProgressSystem.CurrentProgressLevel;
                health.MaxHealth = health.MaxHealth + _healthStep * playerLevel;

                Debug.Log($"maxHealth = {health.MaxHealth}");

                health.OnDie += () =>
                {
                    GamebaseSystems.Instance.GlobalEventsSystem.Invoke(GlobalEventType.PlayerDeath);
                };

                GamebaseSystems.Instance.ProgressSystem.OnProgressLevelChanged += (level) =>
                {
                    RestoreHealth(entity);
                };

                health.Construct();
            }
        }

        private void RestoreHealth(int entity)
        {
            try
            {
                ref var health = ref _healthPool.Get(entity);
                health.Construct();
            }
            catch
            {

            }
        }
    }
}
