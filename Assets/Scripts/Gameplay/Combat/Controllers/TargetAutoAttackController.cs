﻿using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Combat.Events;
using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;
using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Controllers
{
    public class TargetAutoAttackController : IEcsInitSystem, IEcsRunSystem
    {
        EcsWorld _world;
        EcsFilter _filter;
        EcsPool<AttackComponent> _attackComponentsPool;
        EcsPool<AttackTargetComponent> _attackTargetComponentsPool;
        EcsPool<TransformComponent> _transformPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<AttackComponent>()
                .Inc<AttackTargetComponent>()
                .Inc<AutoAttackComponent>()
                .Inc<TransformComponent>()
                .End();
            _attackComponentsPool = _world.GetPool<AttackComponent>();
            _attackTargetComponentsPool = _world.GetPool<AttackTargetComponent>();
            _transformPool = _world.GetPool<TransformComponent>();

            var player = GameObject.FindGameObjectWithTag("Player");
            if (player == null) throw new NullReferenceException();

            foreach (var entity in _filter)
            {
                ref var attackTargetComponent = ref _attackTargetComponentsPool.Get(entity);
                if (!attackTargetComponent.TargetPlayer) continue;

                attackTargetComponent.SetTarget(player.transform);
            }
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var attackComponent = ref _attackComponentsPool.Get(entity);
                ref var attackRange = ref _attackTargetComponentsPool.Get(entity);
                ref var transform = ref _transformPool.Get(entity);

                if (attackComponent.TimeBetweenAttacks == 0) continue;

                float distanceToTarget = Vector3.Distance(transform.Position, attackRange.TargetPosition);

                bool isAttackTime = attackComponent.TimeSinceLastAttack >= attackComponent.TimeBetweenAttacks;
                bool isTargetInRange = distanceToTarget < attackRange.AttackRange;

                if (isAttackTime && isTargetInRange)
                {
                    new AttackEvent(_world, entity, attackComponent.Damage);
                    attackComponent.TimeSinceLastAttack = 0;
                }
                else
                {
                    attackComponent.TimeSinceLastAttack += Time.fixedDeltaTime;
                }
            }
        }
    }
}
