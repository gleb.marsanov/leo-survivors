﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Inventory.Events;
using Assets.Scripts.Gameplay.Weapons.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Controllers
{
    public class EquipWeaponController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<EquipWeaponEvent> _equipWeaponEventsPool;
        private EcsPool<AttackComponent> _attackComponentsPool;
        private EcsPool<WeaponComponent> _weaponsPool;
        private EcsPool<AnimationComponent> _attackAnimationsPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<EquipWeaponEvent>()
                .Inc<AttackComponent>()
                .Inc<AnimationComponent>()
                .End();
            _equipWeaponEventsPool = world.GetPool<EquipWeaponEvent>();
            _attackComponentsPool = world.GetPool<AttackComponent>();
            _weaponsPool = world.GetPool<WeaponComponent>();
            _attackAnimationsPool = world.GetPool<AnimationComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var attackComponent = ref _attackComponentsPool.Get(entity);
                ref var animator = ref _attackAnimationsPool.Get(entity);

                ref var evt = ref _equipWeaponEventsPool.Get(entity);
                if (evt.WeaponEntity != null)
                {
                    ref var weapon = ref _weaponsPool.Get(evt.WeaponEntity.Value);
                    attackComponent.TimeBetweenAttacks = weapon.TimeBetweenAttacks;
                    animator.OverrideAnimator(weapon.AnimationController);
                }
                else
                {
                    attackComponent.TimeBetweenAttacks = 0;
                    animator.OverrideAnimator(null);
                }

                attackComponent.TimeSinceLastAttack = 0;
            }
        }
    }
}
