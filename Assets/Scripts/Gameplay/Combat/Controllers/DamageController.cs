﻿using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Physics.Components;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Combat.Controllers
{
    public class DamageController : IEcsInitSystem
    {
        private static EcsPool<HealthComponent> _healthPool;
        private static EcsPool<CollisionCheckerComponent> _collisionCheckerPool;
        private static EcsPool<DamageComponent> _damagePool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<CollisionCheckerComponent>().Inc<DamageComponent>().End();
            _healthPool = world.GetPool<HealthComponent>();
            _collisionCheckerPool = world.GetPool<CollisionCheckerComponent>();
            _damagePool = world.GetPool<DamageComponent>();

            foreach (var entity in filter)
            {
                InitEntity(entity);
            }
        }

        public static void InitEntity(int entity)
        {
            ref var collisionChecker = ref _collisionCheckerPool.Get(entity);
            ref var damageComponent = ref _damagePool.Get(entity);
            var damage = damageComponent.Damage;

            collisionChecker.OnTriggerEnter += (otherEntity) =>
            {
                TryDealDamage(otherEntity, damage);
            };
        }

        private static void TryDealDamage(int entity, float damage)
        {
            try
            {
                ref var health = ref _healthPool.Get(entity);
                health.TakeDamage(damage);
            }
            catch
            {

            }
        }
    }
}
