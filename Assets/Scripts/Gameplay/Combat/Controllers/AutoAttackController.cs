﻿using Assets.Scripts.Gameplay.Combat.Components;
using Assets.Scripts.Gameplay.Combat.Events;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat.Controllers
{
    public class AutoAttackController : IEcsInitSystem, IEcsRunSystem
    {
        EcsWorld _world;
        EcsFilter _filter;
        EcsPool<AttackComponent> _attackComponentPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<AttackComponent>()
                .Inc<AutoAttackComponent>()
                .Exc<AttackTargetComponent>()
                .End();
            _attackComponentPool = _world.GetPool<AttackComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var attackComponent = ref _attackComponentPool.Get(entity);
                if (attackComponent.TimeBetweenAttacks == 0) continue;

                if (attackComponent.TimeSinceLastAttack >= attackComponent.TimeBetweenAttacks)
                {
                    new AttackEvent(_world, entity, attackComponent.Damage);
                    attackComponent.TimeSinceLastAttack = 0;
                }
                else
                {
                    attackComponent.TimeSinceLastAttack += Time.fixedDeltaTime;
                }
            }
        }
    }
}
