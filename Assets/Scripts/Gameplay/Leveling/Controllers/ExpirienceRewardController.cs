﻿using Assets.Scripts.Gameplay.Combat.Components;
using Gamebase;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Leveling.Controllers
{
    public class ExpirienceRewardController : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<Components.ExpirienceRewardComponent>().Inc<HealthComponent>().End();
            var pool = world.GetPool<Components.ExpirienceRewardComponent>();
            var healthPool = world.GetPool<HealthComponent>();

            GamebaseSystems.Instance.ProgressSystem.OnProgressLevelChanged += (level) => AddAbilityPoint();

            foreach (var entity in filter)
            {
                ref var reward = ref pool.Get(entity);
                ref var health = ref healthPool.Get(entity);
                int rewardPoints = reward.Points;
                health.OnDie += () =>
                {
                    GamebaseSystems.Instance.ProgressSystem.AddXP(rewardPoints);
                };
            }
        }

        private void AddAbilityPoint()
        {
            GamebaseSystems.Instance.ResourcesSystem.Int.Add(ResourceType.AbilityPoints, 1);
        }
    }
}
