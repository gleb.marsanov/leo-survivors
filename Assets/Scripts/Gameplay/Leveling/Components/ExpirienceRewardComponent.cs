﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Leveling.Components
{
    [System.Serializable]
    public struct ExpirienceRewardComponent
    {
        public int Points;
    }
}
