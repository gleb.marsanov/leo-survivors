﻿using Gamebase;

namespace Assets.Scripts.Gameplay.Leveling.Resources
{
    internal class AbilityPointsResource : PlayerPrefsIntResource
    {
        ResourcesSystem _resourcesSystem;

        public AbilityPointsResource(ResourcesSystem resourcesSystem) : base(resourcesSystem)
        {
            _resourcesSystem = resourcesSystem;
        }

        public override ResourceType Type => ResourceType.AbilityPoints;

        protected override int DefaultValue => 0;

        public void AddPoint()
        {
            _resourcesSystem.Int.Add(Type, 1);
        }

        public int GetPointsCount()
        {
            return _resourcesSystem.Int.Get(Type);
        }
    }
}
