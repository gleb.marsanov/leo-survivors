﻿using Zenject;

namespace Assets.Scripts.Gameplay.Leveling.Resources
{
    internal class AbilityPointsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AbilityPointsResource>().AsSingle().NonLazy();
        }
    }
}
