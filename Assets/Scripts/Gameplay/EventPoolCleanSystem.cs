﻿using Assets.Scripts.Gameplay.Combat.Events;
using Assets.Scripts.Gameplay.Inventory.Events;
using Assets.Scripts.Gameplay.Movement.Events;
using Assets.Scripts.Gameplay.Physics.Events;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay
{
    public class EventPoolCleanSystem : IEcsInitSystem, IEcsRunSystem
    {
        EcsWorld _world;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
        }

        public void Run(EcsSystems systems)
        {
            ClearPool<AttackEvent>();

            ClearPool<EnableMovementEvent>();
            ClearPool<DisableMovementEvent>();

            ClearPool<SelectWeaponEvent>();
            ClearPool<EquipWeaponEvent>();

            ClearPool<CollisionEvent>();
        }

        private void ClearPool<T>() where T : struct
        {
            var filter = _world.Filter<T>().End();
            var pool = _world.GetPool<T>();
            foreach (var entity in filter)
            {
                pool.Del(entity);
            }
        }
    }
}
