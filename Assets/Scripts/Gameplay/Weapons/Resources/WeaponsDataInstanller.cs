﻿using Assets.Scripts.Gameplay.Weapons.Resources;
using Zenject;

namespace Assets.Scripts.Gameplay.CoreGameplay.Combat.Data
{
    internal class WeaponsDataInstanller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<WeaponsDataResource>().AsSingle().NonLazy();
        }
    }
}
