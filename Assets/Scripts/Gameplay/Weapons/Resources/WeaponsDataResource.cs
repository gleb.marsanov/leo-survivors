﻿using Gamebase;

namespace Assets.Scripts.Gameplay.Weapons.Resources
{
    internal class WeaponsDataResource : PlayerPrefsStringResource
    {
        public WeaponsDataResource(ResourcesSystem resourcesSystem) : base(resourcesSystem)
        {
        }

        public override ResourceType Type => ResourceType.WeaponsData;
        protected override string DefaultValue => "";
    }
}