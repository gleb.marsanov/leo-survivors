﻿using System.Collections.Generic;

namespace Assets.Scripts.Gameplay.Weapons.Resources
{
    [System.Serializable]
    internal class WeaponsProgressData
    {
        public string[] EquippedWeapons;
        public List<WeaponAbilityData> Weapons;
        public WeaponsProgressData(List<WeaponAbilityData> weaponAbilitiesData, string[] equippedWeapons)
        {
            Weapons = weaponAbilitiesData;
            EquippedWeapons = equippedWeapons;
        }

        public WeaponAbilityData AddWeapon(string guid, int level)
        {
            var abilityData = new WeaponAbilityData(guid, level);
            Weapons.Add(abilityData);
            return abilityData;
        }
    }
}
