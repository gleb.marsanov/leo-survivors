﻿using System;

namespace Assets.Scripts.Gameplay.Weapons.Resources
{
    [System.Serializable]
    internal class WeaponAbilityData
    {
        public string Guid;
        public int Level;

        public WeaponAbilityData(string guid, int level)
        {
            Guid = guid;
            Level = level;
        }
    }
}
