﻿using Assets.Scripts.Gameplay.Weapons.Components;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Weapons.Controllers
{
    public class WeaponColliderController : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<WeaponColliderComponent>().End();
            var pool = world.GetPool<WeaponColliderComponent>();
            foreach (var entity in filter)
            {
                ref var collider = ref pool.Get(entity);
                collider.Construct();
            }
        }
    }
}
