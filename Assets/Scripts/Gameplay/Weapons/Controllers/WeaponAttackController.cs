﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Combat.Events;
using Assets.Scripts.Gameplay.Weapons.Components;
using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Weapons.Controllers
{
    public class WeaponAttackController : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var weaponPool = world.GetPool<WeaponComponent>();

            var filter = world.Filter<WeaponComponent>().Inc<AnimationEventListenerComponent>().End();
            var eventListenerPool = world.GetPool<AnimationEventListenerComponent>();
            foreach (var entity in filter)
            {
                ref var eventListener = ref eventListenerPool.Get(entity);
                ref var weapon = ref weaponPool.Get(entity);
                var weaponDamage = weapon.Damage;

                eventListener.EventListener.OnAttack += () =>
                {
                    new AttackEvent(world, entity, weaponDamage);
                };
            }
        }
    }
}
