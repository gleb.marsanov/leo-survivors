﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Animations.Views;
using Assets.Scripts.Gameplay.Inventory.Components;
using Assets.Scripts.Gameplay.Movement.Views;
using Assets.Scripts.Gameplay.Tags.Components;
using Assets.Scripts.Gameplay.Vfx.Views;
using Assets.Scripts.Gameplay.Weapons.Data;
using Assets.Scripts.Utilities;
using Leopotam.EcsLite;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Controllers
{
    public class PlayerWeaponsInitController : IEcsInitSystem
    {
        private readonly IEnumerable<Weapon> _equippedWeapons;

        public PlayerWeaponsInitController(IEnumerable<Weapon> equippedWeapons)
        {
            _equippedWeapons = equippedWeapons ?? throw new ArgumentNullException();
        }

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var filter = world.Filter<PlayerTagComponent>()
                .Inc<AnimationEventListenerComponent>()
                .Inc<InventoryComponent>()
                .End();
            var inventoryPool = world.GetPool<InventoryComponent>();
            var animationEventListenerPool = world.GetPool<AnimationEventListenerComponent>();


            foreach (var entity in filter)
            {
                ref var inventory = ref inventoryPool.Get(entity);
                ref var animationEventListener = ref animationEventListenerPool.Get(entity);

                foreach (var weapon in _equippedWeapons)
                {
                    if (TryInstantiateWeapon(weapon, ref inventory, ref animationEventListener, out var weaponObject))
                    {
                        EntityConverter.TryConvert(world, weaponObject, out var weaponEntity);
                        inventory.AddWeapon(weaponEntity);
                    }
                    else
                    {
                        inventory.AddWeapon(null);
                    }
                }
            }
        }

        private static bool TryInstantiateWeapon(Weapon weapon, ref InventoryComponent inventory, ref AnimationEventListenerComponent animationEventListener, out GameObject weaponObject)
        {
            if (weapon == null)
            {
                weaponObject = null;
                return false;
            }

            var currentLevel = weapon.CurrentLevel;
            weaponObject = GameObject.Instantiate(currentLevel.prefab,
                weapon.spawnInRightHand ? inventory.RightHand : inventory.LeftHand);
            weaponObject.GetComponent<AnimationEventListenerView>().Value.EventListener = animationEventListener.EventListener;
            weaponObject.GetComponent<TransformView>().Value.SetTransform(weaponObject.transform);
            weaponObject.GetComponent<AttackVfx>().Value.SetSpawnPoint(inventory.ModelTransform);
            weaponObject.SetActive(false);

            return true;
        }
    }
}
