﻿using Assets.Scripts.Gameplay.Vfx.Views;
using Assets.Scripts.Gameplay.Weapons.Views;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Data
{
    [System.Serializable]
    public class WeaponLevel
    {
        [HideLabel]
        [HorizontalGroup("row1", 100), VerticalGroup("row1/left")]
        [PreviewField(100, ObjectFieldAlignment.Right)]
        [OnValueChanged(nameof(OnChangePrefab))]
        public GameObject prefab;


        [VerticalGroup("row1/right")]
        [OnValueChanged(nameof(OnChangeDamage))]
        public float damage;
        [VerticalGroup("row1/right")]
        [OnValueChanged(nameof(OnChangeTimeBetweenAttacks))]
        public float timeBetweenAttacks;
        [VerticalGroup("row1/right"), OnValueChanged(nameof(OnChangeVfx))]
        public GameObject attackVfxPrefab;

        void OnChangePrefab()
        {
            var weaponComponent = prefab.GetComponent<WeaponView>();
            damage = weaponComponent.Value.Damage;
            timeBetweenAttacks = weaponComponent.Value.TimeBetweenAttacks;
            attackVfxPrefab = prefab.GetComponent<AttackVfx>().Value.Vfx;
#if UNITY_EDITOR
            PrefabUtility.RecordPrefabInstancePropertyModifications(prefab);
#endif
        }

        void OnChangeDamage()
        {
            var weaponComponent = prefab.GetComponent<WeaponView>();
            weaponComponent.Value.Damage = damage;
#if UNITY_EDITOR
            PrefabUtility.RecordPrefabInstancePropertyModifications(prefab);
#endif
        }

        void OnChangeTimeBetweenAttacks()
        {
            var weaponComponent = prefab.GetComponent<WeaponView>();
            weaponComponent.Value.TimeBetweenAttacks = timeBetweenAttacks;
#if UNITY_EDITOR
            PrefabUtility.RecordPrefabInstancePropertyModifications(prefab);
#endif
        }

        void OnChangeVfx()
        {
            var weaponComponent = prefab.GetComponent<AttackVfx>();
            weaponComponent.Value.Vfx = attackVfxPrefab;
#if UNITY_EDITOR
            PrefabUtility.RecordPrefabInstancePropertyModifications(prefab);
#endif
        }
    }
}
