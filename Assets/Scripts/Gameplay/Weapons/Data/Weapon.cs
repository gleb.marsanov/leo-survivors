﻿using System.Collections.Generic;
using Assets.Scripts.Gameplay.CoreGameplay.Combat.Data;
using Assets.Scripts.Gameplay.Weapons.Views;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Data
{
    public class Weapon : ScriptableObject
    {
        [HideLabel]
        [HorizontalGroup("row1", 100), VerticalGroup("row1/left")]
        [PreviewField(100, ObjectFieldAlignment.Left)]
        public Sprite icon;

        [VerticalGroup("row1/right", 300)]
        [HideInInspector] public string guid;

        [VerticalGroup("row1/right", 300)]
        public AnimatorOverrideController animatorOverride;

        [VerticalGroup("row1/right")]
        public bool spawnInRightHand;

        [VerticalGroup("row1/right")]
        public int currentLevel;

        public List<WeaponLevel> levels;

        public WeaponLevel CurrentLevel => levels[currentLevel - 1];

        private void OnChangeAnimator()
        {
            foreach (var weaponLevel in levels)
            {
                weaponLevel.prefab.GetComponent<WeaponView>().Value.AnimationController = animatorOverride;
            }
        }
    }
}
