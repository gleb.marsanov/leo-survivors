﻿using Assets.Scripts.Gameplay.Weapons.Resources;
using Gamebase;
using Sirenix.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Data
{
    public class WeaponsDataParser : IDisposable
    {
        private WeaponsProgressData _progressData;
        private readonly WeaponsSettings _weaponsSettings;
        private Weapon[] _equippedWeapons;
        public IEnumerable<Weapon> EquippedWeapons => _equippedWeapons;
        public IEnumerable<Weapon> Weapons => _weaponsSettings.playerWeapons;

        public WeaponsDataParser(WeaponsSettings weaponsSettings)
        {
            _weaponsSettings = weaponsSettings ?? throw new ArgumentNullException();

            string json = GamebaseSystems.Instance.ResourcesSystem.String.Get(ResourceType.WeaponsData);
            OnChangeResource(json);

            GamebaseSystems.Instance.ResourcesSystem.String.Subscribe(ResourceType.WeaponsData, OnChangeResource);
        }

        private Weapon[] GetEquippedWeapons(IEnumerable<string> weaponsGuid, IEnumerable<Weapon> weapons)
        {
            var equippedWeapons = new Weapon[weaponsGuid.Count()];
            for (int i = 0; i < equippedWeapons.Count(); i++)
            {
                equippedWeapons[i] = weapons.FirstOrDefault(weapon => weaponsGuid.ElementAt(i) == weapon.guid);
            }

            return equippedWeapons;
        }

        private void LoadWeaponsLevels(IEnumerable<Weapon> weapons)
        {
            weapons.ForEach((weapon) =>
            {
                var weaponData = _progressData.Weapons.FirstOrDefault(u => u.Guid == weapon.guid) ??
                                 _progressData.AddWeapon(weapon.guid, 0);

                weapon.currentLevel = weaponData.Level;
            });
        }

        private WeaponsProgressData Parse(string json)
        {
            var data = JsonUtility.FromJson<WeaponsProgressData>(json);
            if (data?.Weapons == null)
                data = new WeaponsProgressData(new List<WeaponAbilityData>(), new string[3]);

            return data;
        }

        public void EquipWeapon(Weapon weapon, int slotId)
        {
            _equippedWeapons[slotId] = weapon;
        }

        public void Save()
        {
            _weaponsSettings.playerWeapons.ForEach((weapon) =>
            {
                var weaponData = _progressData.Weapons.FirstOrDefault(u => u.Guid == weapon.guid);
                weaponData.Level = weapon.currentLevel;
            });

            for (int i = 0; i < _equippedWeapons.Length; i++)
            {
                _progressData.EquippedWeapons[i] = _equippedWeapons[i]?.guid;
            }

            var json = JsonUtility.ToJson(_progressData);
            GamebaseSystems.Instance.ResourcesSystem.String.Set(ResourceType.WeaponsData, json);
        }

        public void Reset()
        {
            GamebaseSystems.Instance.ResourcesSystem.String.Set(ResourceType.WeaponsData, "");
        }

        private void OnChangeResource(string json)
        {
            _progressData = Parse(json);
            _equippedWeapons = GetEquippedWeapons(_progressData.EquippedWeapons, _weaponsSettings.playerWeapons);
            LoadWeaponsLevels(_weaponsSettings.playerWeapons);
        }

        public void Dispose()
        {
            GamebaseSystems.Instance.ResourcesSystem.String.Unsubscribe(ResourceType.WeaponsData, OnChangeResource);
        }
    }
}
