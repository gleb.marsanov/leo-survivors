﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Data
{
    [CreateAssetMenu(fileName = "PlayerWeaponsData", menuName = "ScriptableObjects/PlayerWeaponsData", order = 1)]
    public class WeaponsSettings : ScriptableObject
    {
        public const string ASSET_PATH = "Assets/Resources/Settings/Weapons.asset";

        [HideInInspector] public List<Weapon> playerWeapons = new List<Weapon>();
    }
}
