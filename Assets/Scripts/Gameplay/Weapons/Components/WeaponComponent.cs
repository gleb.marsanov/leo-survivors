﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Components
{
    [System.Serializable]
    public struct WeaponComponent
    {
        public float Damage;
        public float TimeBetweenAttacks;
        public AnimatorOverrideController AnimationController;
    }
}
