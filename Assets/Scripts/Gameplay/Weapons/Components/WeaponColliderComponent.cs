﻿using Assets.Scripts.Gameplay.Animations.Providers;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Components
{
    [System.Serializable]
    internal struct WeaponColliderComponent
    {
        [SerializeField] private Collider _collider;
        [SerializeField] private AnimationEventListener _animationEventListener;

        public void Construct()
        {
            _animationEventListener.OnEnableWeaponCollider += EnableCollider;
            _animationEventListener.OnDisableWeaponCollider += DisableCollider;
        }

        public void SetAnimationEventListener(AnimationEventListener animationEventListener)
        {
            _animationEventListener.OnEnableWeaponCollider -= EnableCollider;
            _animationEventListener.OnDisableWeaponCollider -= DisableCollider;
            _animationEventListener = animationEventListener;
            Construct();
        }

        private void EnableCollider()
        {
            _collider.enabled = true;
        }

        private void DisableCollider()
        {
            _collider.enabled = false;
        }
    }
}
