﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Weapons.Decorators
{
    internal abstract class WeaponDecorator : WeaponBase
    {
        private WeaponBase _weapon;

        protected WeaponDecorator(EcsWorld ecsWorld, WeaponBase weapon) : base(weapon.Entity)
        {
            _weapon = weapon;
        }
    }
}
