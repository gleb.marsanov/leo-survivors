﻿namespace Assets.Scripts.Gameplay.Weapons.Decorators
{
    internal abstract class WeaponBase
    {
        protected WeaponBase(int entity)
        {
            Entity = entity;
        }

        public int Entity { get; }
    }
}
