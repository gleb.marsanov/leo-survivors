﻿using Assets.Scripts.Gameplay.Animations.Components;
using Assets.Scripts.Gameplay.Animations.Providers;
using Assets.Scripts.Gameplay.Vfx.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Decorators
{
    internal class AttackVfxDecorator : WeaponDecorator
    {
        public AttackVfxDecorator(EcsWorld ecsWorld, WeaponBase weapon, GameObject vfxPrefab, Transform spawnPoint,
            bool spawnInWorldSpace, bool spawnAtMousePosition, AnimationEventListener animationEventListener) : base(ecsWorld, weapon)
        {
            ref var attackVfxComponent = ref ecsWorld.GetPool<AttackVfxComponent>().Add(Entity);
            attackVfxComponent.SetSpawnPoint(spawnPoint);
            attackVfxComponent.Vfx = vfxPrefab;
            attackVfxComponent.ShouldSpawnInWorldSpace(spawnInWorldSpace);
            attackVfxComponent.ShouldSpawnAtMousePosition(spawnAtMousePosition);

            ref var animationEventListenerComponent = ref ecsWorld.GetPool<AnimationEventListenerComponent>().Add(Entity);
            animationEventListenerComponent.EventListener = animationEventListener;
        }
    }
}
