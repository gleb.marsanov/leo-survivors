﻿using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Gameplay.Weapons.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Weapons.Decorators
{
    internal class Weapon : WeaponBase
    {
        public Weapon(
            EcsWorld ecsWorld,
            AnimatorOverrideController animationController,
            float damage,
            float timeBetweenAttacks,
            Transform weaponTransform
            ) : base(ecsWorld.NewEntity())
        {
            ref var weapon = ref ecsWorld.GetPool<WeaponComponent>().Add(Entity);
            weapon.AnimationController = animationController;
            weapon.Damage = damage;
            weapon.TimeBetweenAttacks = timeBetweenAttacks;

            ref var transform = ref ecsWorld.GetPool<TransformComponent>().Add(Entity);
            transform.SetTransform(weaponTransform);
        }
    }
}
