﻿using Assets.Scripts.Gameplay.Weapons.Components;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Gameplay.Weapons.Views
{
    public class WeaponView : MonoProvider<WeaponComponent>
    {
        public ref WeaponComponent Value => ref value;
    }
}
