﻿using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Gameplay.Rotation.Components;
using Leopotam.EcsLite;
using System;
using Assets.Scripts.Gameplay.Animations.Components;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Controllers
{
    public class RotationToTargetController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<RotationToTargetComponent> _rotatorPool;
        private Transform _player;
        private EcsPool<TransformComponent> _transformPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<RotationToTargetComponent>().Inc<TransformComponent>().Inc<AnimationEventListenerComponent>().End();
            _rotatorPool = world.GetPool<RotationToTargetComponent>();
            _transformPool = world.GetPool<TransformComponent>();
            var animationEventListenerPool = world.GetPool<AnimationEventListenerComponent>();

            var playerGameObject = GameObject.FindGameObjectWithTag("Player");
            if (playerGameObject == null) throw new NullReferenceException();
            _player = playerGameObject.transform;

            foreach (var entity in _filter)
            {
                ref var rotator = ref _rotatorPool.Get(entity);
                if (rotator.RotateToPlayer)
                    rotator.SetTarget(_player);

                rotator.EnableRotation();
                if (rotator.DisableRotationOnAttack)
                {
                    ref var animationEventListener = ref animationEventListenerPool.Get(entity);
                    animationEventListener.EventListener.OnDisableMovement += () => DisableRotation(entity);
                    animationEventListener.EventListener.OnEnableMovement += () => EnableRotation(entity);
                }
            }
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var rotator = ref _rotatorPool.Get(entity);
                ref var transform = ref _transformPool.Get(entity);

                var loockDirection = new Vector3(rotator.TargetPosition.x, transform.Position.y, rotator.TargetPosition.z);
                rotator.LookAt(loockDirection);
            }
        }

        private void DisableRotation(int entity)
        {
            ref var rotator = ref _rotatorPool.Get(entity);
            rotator.DisableRotation();
        }
        private void EnableRotation(int entity)
        {
            ref var rotator = ref _rotatorPool.Get(entity);
            rotator.EnableRotation();
        }
    }
}
