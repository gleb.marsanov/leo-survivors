﻿using Assets.Scripts.Gameplay.Movement.Components;
using Assets.Scripts.Gameplay.Rotation.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Controllers
{
    public class RotationToDirectionController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<RotationToDirectionComponent> _rotationPool;
        private EcsPool<MovementComponent> _moverPool;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<RotationToDirectionComponent>().Inc<MovementComponent>().End();
            _rotationPool = world.GetPool<RotationToDirectionComponent>();
            _moverPool = world.GetPool<MovementComponent>();
            
            foreach (var entity in _filter)
            {
                ref var rotator = ref _rotationPool.Get(entity);
                if (rotator.DisableRotationOnAttack)
                {
                    rotator.OnDisableRotation += ()=> DisableRotation(entity);
                    rotator.OnEnableRotation += () => EnableRotation(entity);
                }
            }
        }
        
        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var rotator = ref _rotationPool.Get(entity);
                ref var mover = ref _moverPool.Get(entity);
                if (mover.Direction != Vector3.zero)
                    rotator.RotateToDirection(mover.Direction);
            }
        }
        private void DisableRotation(int entity)
        {
            ref var rotator = ref _rotationPool.Get(entity);
            rotator.IsRotationEnabled = false;
        }

        private void EnableRotation(int entity)
        {
            ref var rotator = ref _rotationPool.Get(entity);
            rotator.IsRotationEnabled = true;
        }
    }
}
