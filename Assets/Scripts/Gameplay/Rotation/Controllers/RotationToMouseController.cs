﻿using Assets.Scripts.Gameplay.Rotation.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Controllers
{
    public class RotationToMouseController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter _filter;
        private EcsPool<RotationToMouseComponent> _pool;
        private Camera _mainCamera;

        public void Init(EcsSystems systems)
        {
            var world = systems.GetWorld();
            _filter = world.Filter<RotationToMouseComponent>().End();
            _pool = world.GetPool<RotationToMouseComponent>();
            _mainCamera = Camera.main;
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                RotateToMouse(entity);
            }
        }

        private void RotateToMouse(int entity)
        {
            ref var rotator = ref _pool.Get(entity);
            var mousePosition = _mainCamera.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            Ray cameraRay = _mainCamera.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit hit;
            if (UnityEngine.Physics.Raycast(cameraRay, out hit))
            {
                Vector3 pointToLook = hit.point;
                Debug.DrawLine(cameraRay.origin, pointToLook, Color.cyan);

                rotator.LookAt(new Vector3(pointToLook.x, rotator.ModelPosition.y, pointToLook.z));
            }
        }
    }
}
