﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Rotation.Events
{
    internal struct DisableRotationEvent
    {
        public DisableRotationEvent(EcsWorld world, int entity)
        {
            world.GetPool<DisableRotationEvent>().Add(entity);
        }
    }
}
