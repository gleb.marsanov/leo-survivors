﻿using Assets.Scripts.Gameplay.Animations.Providers;
using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Utilities
{
    [System.Serializable]
    internal class Rotator
    {
        [SerializeField] private Transform _modelTransform;
        [SerializeField] private float _timeToReachTargetRotation;
        private float _dampedTargetRotationCurrentVelocity;
        private float _dampedTargetRotationPassedTime;
        private float _currentTargetRotation;
        public AnimationEventListener AnimationEventListener;
        

        public void Rotate(Vector3 direction)
        {
            RotateToDirection(direction);
        }

        private void RotateToDirection(Vector3 direction)
        {
            float directionAngle = UpdateTargetRotation(direction);

            RotateTowardsTargetRotation();
        }

        private float UpdateTargetRotation(Vector3 direction)
        {
            float directionAngle = MathfUtility.GetDirectionAngle(direction);
            directionAngle = MathfUtility.AddCameraRotationToAngle(directionAngle);

            if (directionAngle != _currentTargetRotation)
            {
                UpdateTargetRotationData(directionAngle);
            }

            return directionAngle;
        }

        private void RotateTowardsTargetRotation()
        {
            float currentYAngle = _modelTransform.rotation.eulerAngles.y;
            if (currentYAngle == _currentTargetRotation) return;

            float smoothedYAngle = Mathf.SmoothDampAngle(
                currentYAngle,
                _currentTargetRotation,
                ref _dampedTargetRotationCurrentVelocity,
                _timeToReachTargetRotation - _dampedTargetRotationPassedTime);

            _dampedTargetRotationPassedTime += Time.deltaTime;

            Quaternion targetRotation = Quaternion.Euler(0f, smoothedYAngle, 0f);

            _modelTransform.rotation = targetRotation;
        }

        private void UpdateTargetRotationData(float directionAngle)
        {
            _currentTargetRotation = directionAngle;
            _dampedTargetRotationPassedTime = 0f;
        }
    }
}
