﻿using Assets.Scripts.Gameplay.Rotation.Utilities;
using System;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Components
{
    [System.Serializable]
    internal struct RotationToDirectionComponent
    {
        [SerializeField] private Rotator _rotator;
        public bool DisableRotationOnAttack;
        public bool IsRotationEnabled;

        public Action OnEnableRotation
        {
            get => _rotator.AnimationEventListener.OnEnableMovement;
            set => _rotator.AnimationEventListener.OnEnableMovement = value;
        }

        public Action OnDisableRotation
        {
            get => _rotator.AnimationEventListener.OnDisableMovement;
            set => _rotator.AnimationEventListener.OnDisableMovement = value;
        }

        public void RotateToDirection(Vector3 direction)
        {
            if (IsRotationEnabled)
                _rotator.Rotate(direction);
        }
    }
}
