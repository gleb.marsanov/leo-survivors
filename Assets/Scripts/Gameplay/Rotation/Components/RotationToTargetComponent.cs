﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Components
{
    [System.Serializable]
    internal struct RotationToTargetComponent
    {
        [SerializeField] private bool _rotateToPlayer;
        [SerializeField, HideIf(nameof(_rotateToPlayer))] private Transform _target;
        [SerializeField, Required] private Transform _modelTransform;
        public bool DisableRotationOnAttack;
        private bool _isRotationEnabled;

        public Vector3 TargetPosition => _target.position;

        public bool RotateToPlayer { get => _rotateToPlayer; set => _rotateToPlayer = value; }

        public void SetTarget(Transform target)
        {
            _target = target;
        }

        public void LookAt(Vector3 targetPosition)
        {
            if (_isRotationEnabled)
                _modelTransform.LookAt(targetPosition);
        }

        public void EnableRotation()
        {
            _isRotationEnabled = true;
        }

        public void DisableRotation()
        {
            _isRotationEnabled = false;
        }
    }
}
