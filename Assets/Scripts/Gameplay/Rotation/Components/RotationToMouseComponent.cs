﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Rotation.Components
{
    [System.Serializable]
    internal struct RotationToMouseComponent
    {
        [SerializeField] private Transform _modelTransform;
        [SerializeField, Range(0.01f, 1f)] private float _lerp;

        public Vector3 ModelPosition => _modelTransform.position;

        public void LookAt(Vector3 direction)
        {
            var targetRotation = Quaternion.LookRotation(direction - _modelTransform.position);
            Quaternion rotation = Quaternion.Lerp(_modelTransform.rotation, targetRotation, _lerp);

            _modelTransform.rotation = rotation;
        }
    }
}
