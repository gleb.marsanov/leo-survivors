﻿using Assets.Scripts.Gameplay.Inventory.Components;
using Assets.Scripts.Gameplay.Inventory.Events;
using Assets.Scripts.Gameplay.Movement.Components;
using Leopotam.EcsLite;
using System.Linq;

namespace Assets.Scripts.Gameplay.Inventory.Controllers
{
    public class InventoryController : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<InventoryComponent> _inventoryPool;
        private EcsPool<SelectWeaponEvent> _selectWeaponEventsPool;
        private EcsPool<TransformComponent> _transformPool;
        private EcsPool<MovementComponent> _movementComponentPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<InventoryComponent>().Inc<SelectWeaponEvent>().Inc<MovementComponent>().End();
            _selectWeaponEventsPool = _world.GetPool<SelectWeaponEvent>();
            _inventoryPool = _world.GetPool<InventoryComponent>();
            _transformPool = _world.GetPool<TransformComponent>();
            _movementComponentPool = _world.GetPool<MovementComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var mover = ref _movementComponentPool.Get(entity);
                if (!mover.CanMove) continue;

                ref var inventory = ref _inventoryPool.Get(entity);
                var evt = _selectWeaponEventsPool.Get(entity);

                if (inventory.CurrentWeapon != null)
                {
                    ref var currentWeaponTransform = ref _transformPool.Get(inventory.CurrentWeapon.Value);
                    currentWeaponTransform.SetActive(false);
                }

                var newWeapon = inventory.Weapons.ElementAt(evt.SlotId);
                if (newWeapon != null)
                {
                    ref var newWeaponTransform = ref _transformPool.Get(newWeapon.Value);
                    newWeaponTransform.SetActive(true);
                }

                inventory.CurrentWeapon = newWeapon;
                new EquipWeaponEvent(_world, entity, newWeapon);
            }
        }
    }
}
