﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Inventory.Events
{
    internal struct SelectWeaponEvent
    {
        private int _slotId;

        public SelectWeaponEvent(EcsWorld world, int entity, int slotId)
        {
            ref var evt = ref world.GetPool<SelectWeaponEvent>().Add(entity);
            evt._slotId = slotId;
            this = evt;
        }

        public int SlotId => _slotId;
    }
}
