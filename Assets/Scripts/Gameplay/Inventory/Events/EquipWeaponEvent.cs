﻿using Leopotam.EcsLite;

namespace Assets.Scripts.Gameplay.Inventory.Events
{
    internal struct EquipWeaponEvent
    {
        private int? _weaponEntity;

        public EquipWeaponEvent(EcsWorld world, int entity, int? weaponEntity)
        {
            ref var evt = ref world.GetPool<EquipWeaponEvent>().Add(entity);
            evt._weaponEntity = weaponEntity;
            this = evt;
        }

        public int? WeaponEntity => _weaponEntity;
    }
}
