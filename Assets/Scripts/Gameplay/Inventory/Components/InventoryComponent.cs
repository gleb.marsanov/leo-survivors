﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Gameplay.Inventory.Components
{
    [System.Serializable]
    internal struct InventoryComponent
    {
        public Transform RightHand;
        public Transform LeftHand;
        public Transform ModelTransform;
        public int? CurrentWeapon;
        private List<int?> _weapons;

        public IEnumerable<int?> Weapons => _weapons;

        public void AddWeapon(int? weapon)
        {
            _weapons ??= new List<int?>();
            _weapons.Add(weapon);

            Debug.Log($"Inventory add weapon {weapon}");
        }
    }
}
