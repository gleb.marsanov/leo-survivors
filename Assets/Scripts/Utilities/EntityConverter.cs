﻿using System.Linq;
using Leopotam.EcsLite;
using UnityEngine;
using Voody.UniLeo.Lite;

namespace Assets.Scripts.Utilities
{
    public class EntityConverter
    {
        public static bool TryConvert(EcsWorld world, GameObject gameObject, out int entity, bool checkChildren = true)
        {
            if (gameObject.GetComponent<ConvertToEntity>() == null)
            {
                entity = -1;
                return false;
            }

            entity = world.NewEntity();

            foreach (var component in gameObject.GetComponents<Component>())
            {
                if (component is IConvertToEntity entityComponent)
                {
                    entityComponent.Convert(entity, world);
                    GameObject.Destroy(component);
                }
            }

            if (checkChildren)
            {
                var chiildren = gameObject.GetComponentsInChildren<ConvertToEntity>()
                    .Where(u => u.transform != gameObject.transform);
                foreach (var child in chiildren)
                {
                    TryConvert(world, child.gameObject, out int childEntity, false);
                }
            }

            return true;
        }
    }
}
