﻿using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public static class MathfUtility
    {
        public static float GetDirectionAngle(Vector3 direction)
        {
            float directionAngle = UnityEngine.Mathf.Atan2(direction.x, direction.z) * UnityEngine.Mathf.Rad2Deg;

            if (directionAngle < 0)
            {
                directionAngle += 360f;
            }

            return directionAngle;
        }

        public static float AddCameraRotationToAngle(float angle)
        {
            angle += Camera.main.transform.eulerAngles.y;

            if (angle > 360f)
            {
                angle -= 360f;
            }

            return angle;
        }
    }

}
